<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class ResultReading extends Model
{
    protected $table = 'result_reading';
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function results()
    {
        return $this->belongsTo('App\Models\Result', 'result_id');
    }

    public function result_cards()
    {
        return $this->belongsTo('App\Models\DeckCard', 'deck_id');
    }

    public function result_readings()
    {
        return $this->belongsTo('App\Models\Reading', 'reading_id');
    }
    
    public function created_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function save(array $options = Array())
    {
        // before save code 
    	// if(Auth::check())
    	// {
	    //     $id = Auth::id();
	    //     if ( $this->exists == '' ) {
	    //         $this->created_by = $id;
	    //     } else {
	    //         $this->updated_by = $id;
	    //     }
    	// }
        parent::save();
        // after save code
    }
}
