<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class Result extends Model
{
    protected $table = 'result';
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function result_user()
    {
        return $this->belongsTo('App\Models\UserProfile', 'user_id');
    }

    public function result_reading()
    {
        return $this->hasMany('App\Models\ResultReading', 'result_id');
    }
    
    public function created_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function save(array $options = Array())
    {
        // before save code 
    	// if(Auth::check())
    	// {
	    //     $id = Auth::id();
	    //     if ( $this->exists == '' ) {
	    //         $this->created_by = $id;
	    //     } else {
	    //         $this->updated_by = $id;
	    //     }
    	// }
        parent::save();
        // after save code
    }
}
