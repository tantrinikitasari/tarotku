<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class Profile extends Model
{
    protected $table = 'user_profile';
    protected $fillable = ['user_id', 'fullname', 'birthday','address','phone','gender','pic'];
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function owners()
    {
        return $this->hasOne('App\User', 'user_id');
    }

    public function user_result()
    {
        return $this->hasMany('App\Models\Result', 'user_id');
    }
    
    public function created_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function save(array $options = Array())
    {
        // before save code 
        parent::save();
        // after save code
    }
}
