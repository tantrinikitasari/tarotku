<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class Configuration extends Model
{
    protected $table = 'configuration';
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function created_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function save(array $options = Array())
    {
        // before save code 
        $id = Auth::id();
        if ( $this->exists == '' ) {
            $this->created_by = $id;
        } else {
            $this->updated_by = $id;
        }
        parent::save();
        // after save code
    }
}
