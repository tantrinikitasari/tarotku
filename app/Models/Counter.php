<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class Counter extends Model
{
    protected $table = 'counter';
    protected $dates =['deleted_at'];
    use SoftDeletes;


	public function save(array $options = Array())
    {
        // before save code 
    	if(Auth::check())
    	{
	        $id = Auth::id();
	        if ( $this->exists == '' ) {
	            $this->created_by = $id;
	        } else {
	            $this->updated_by = $id;
	        }
    	}
        parent::save();
        // after save code
    }
}
