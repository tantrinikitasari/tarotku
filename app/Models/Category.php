<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class Category extends Model
{
    protected $table = 'category';
    protected $dates =['deleted_at'];
    use SoftDeletes;

    public function scopeStatus($query, $status)
    {	
    	return $query->where('status','=',$status);
    }

	public function scopeMaxCards($query, $max)
	{
	   	return $query->where('max','=',$max);
	}

    public function category_reading()
    {
        return $this->hasMany('App\Models\Reading', 'category_id');
    }

    public function created_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

	public function save(array $options = Array())
    {
        // before save code 
    	if(Auth::check())
    	{
	        $id = Auth::id();
	        if ( $this->exists == '' ) {
	            $this->created_by = $id;
	        } else {
	            $this->updated_by = $id;
	        }
    	}
        parent::save();
        // after save code
    }
}
