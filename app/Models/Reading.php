<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;	
use Auth;

class Reading extends Model
{    
    protected $table = 'reading';
    protected $dates =['deleted_at'];
    use SoftDeletes;

    public function scopeReadingCard($query, $category)
    {
    	return $query->where('category_id','=',$category);
    }
    
    public function reading_category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function result_readings()
    {
        return $this->belongsToMany('App\Models\ResultReading', 'reading_id');
    }
    
    public function created_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function save(array $options = Array())
    {
        // before save code 
        $id = Auth::id();
        if ( $this->exists == '' ) {
            $this->created_by = $id;
        } else {
            $this->updated_by = $id;
        }
        parent::save();
        // after save code
    }
}

