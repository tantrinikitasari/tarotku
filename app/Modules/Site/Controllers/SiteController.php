<?php namespace App\Modules\Site\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Database\Query;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Paginator;

use DB;
use Auth;
use Validator;

use App\Models\DeckCard;
use App\Models\Category;
use App\Models\Reading;
use App\Models\Counter;
use App\Models\Result;
use App\Models\ResultReading;
use App\Models\Profile;


class SiteController extends Controller {

	protected $layout = 'layout';
	protected $mdls = 'Site';
	protected $nest = 'site';

	public function __construct(){
		$this->resource = (object) array(
			'layout' 		=> $this->layout,
			'mdls' 			=> $this->mdls,
			'nest' 			=> $this->nest,
		);
	}

	public function index(){

        $cardall = DeckCard::select('id')->get();

        if(Auth::check()){
            $pilihan = Category::Status(1)->get();
    	}else{
            $pilihan = Category::Status(0)->get();
    	}
   
        return view( $this->mdls . "::" . $this->nest . ".welcome")->with('cardall',$cardall)->with('pilihan',$pilihan);

	}

	public function reading(Request $request){

		$count_lang = count($request->input('card_selected'));
        foreach (range(0, $count_lang - 1) as $number) {
            $rules['card_selected.' . $number] = 'required|numeric';
            $messages['card_selected.' . $number .'.required'] = 'Card field is required';
            $messages['card_selected.' . $number .'.numeric'] = 'Card must be a number';
        }

        $validator = Validator::make( $request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('index')
                        ->withErrors($validator);
        } else {

            if(Auth::check()){
                $status = 1;
                $user_id = Auth::user()->id;
                $username = Auth::user()->username;
            }else{
                $x = Counter::max($columns ='no');
                $status = 0;
                $user_id = 0;
                $username = 'Guest_'.$x;    
                
                $upd_counter = Counter::find(1);
                $upd_counter->no = $x+1;
                $upd_counter->save();
            }            
            
            $result                      = new Result;
            $result->key                 = date('Ymdhis').str_random(10);
            $result->user_id             = $user_id;
            $result->username            = $username;
            $result->save();

            $totalcard = count($request->input('card_selected'));       
            $category = Category::Status($status)->maxCards($totalcard)->first();

            if($category) {
                $ids_ordered = implode(',', $request->input('card_selected'));
                $reading = Reading::ReadingCard($category->id)->get();
                $cardreading = DeckCard::whereIn('id',$request->input('card_selected'))
                            ->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
                            ->get();

                foreach ($reading as $key => $read){
                    $data                      = new ResultReading;
                    $data->result_id           = $result->id;
                    $data->reading_id          = $read->id;
                    $data->deck_id             = $cardreading[$key]->id;
                    $data->save();
                }

       		}else {

       		}

            return view( $this->mdls . "::" . $this->nest . ".reading")->with('reading',$reading)->with('cardreading',$cardreading);
			
        }
        	
	}

    public function profile(Request $request)
    {
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $prof = Profile::where('id',$user_id)->first();
            
        }else{
           
        }

        return view( $this->mdls . "::" . $this->nest . ".profile")->with('prof',$prof);
    }

    public function editprofile(Request $request)
    {
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $prof = Profile::where('id',$user_id)->first();         
        }

        return view( $this->mdls . "::" . $this->nest . ".editprofile")->with('prof',$prof);
    }

    public function saveprofile(Request $request)
    {
        if(Auth::check()){
            $user_id = Auth::user()->id;       
        }

        $prof = Profile::where('id',$user_id)->first();
        $profile = Profile::find($prof);
        
        if($request->hasFile('pic'))
           {       
            $file = $request->file('pic');
            $filename = str_replace(array('/', '\\', '..'), '', $file->getClientOriginalName());
            $extension = substr($filename, strrpos($filename, '.') + 1);

            $filename = sprintf('%d_%s.%s', $prof->id, str_random(5), $extension);
            $file->move('assets/profilepic/', $filename);

            $path = 'assets/profilepic/';
            $file = $prof->pic;

            $files = new Filesystem;
            $files->delete($path.$file);

            $prof->pic = $filename;
           }
        
        $prof->update();
        return redirect('/profile');
    
    }

    public function histori(Request $request)
    {
        if(Auth::check()){
            $users = Auth::user()->username;       
        }

        $result = Result::select('result.*','category.*','result_reading.*')
                ->leftJoin('result_reading', 'result.id' ,'=', 'result_reading.result_id')
                ->leftJoin('reading', 'reading.id' ,'=', 'result_reading.reading_id')
                ->leftJoin('category', 'category.id' ,'=', 'reading.category_id')
                ->where('username',$users)
                ->groupBy('result.key')
                ->paginate(10);

        return view( $this->mdls . "::" . $this->nest . ".histori")->with('result',$result);
    
    }

    public function historiramalan($key)
    {
        if(Auth::check()){
            $users = Auth::user()->username;       
        }

        if($key==''){
            echo "error";
        }

        $reading = Result::select('result_reading.*','deck_card.cardname','deck_card.description as card_desc','deck_card.imagecard','reading.description as reading_desc')
                    ->leftJoin('result_reading','result.id','=','result_reading.result_id')
                    ->leftJoin('reading','result_reading.reading_id','=','reading.id')
                    ->leftJoin('deck_card','result_reading.deck_id','=','deck_card.id')
                    ->where('result.key','=',$key)
                    ->get();
  
        return view( $this->mdls . "::" . $this->nest . ".historireading")->with('reading',$reading);

    }

}
