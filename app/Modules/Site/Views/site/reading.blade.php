@extends('layout')

@section('title')Tarotku @endsection

@section('metadata')
	@include( 'Site::metadata' )
@endsection

@section('include') 
	@include( 'Site::include' )
@endsection

@section('header')
	@include( 'Site::header' )
@endsection

@section('navigation') 
	@include( 'Site::navigation' )
@endsection

@section('content')

<div class="choose-outer">
		<div class="container whitebackground">
			<div class="page-header"> 
				<h2 style="text-align: center">Hasil Ramalanmu</h2>
			</div> 
			@if($reading)
				@foreach ($reading as $key => $read)
				    <div class="row marginrow" >
				    	<div class="col-sm-12 col-md-4 col-md-3 reading-card-wrap" style="text-align: center">
							<a class="image-popup-no-margins" href="{{ asset('assets/card/'.$cardreading[$key]->imagecard) }}">
								<img src="{{ asset('assets/card/'.$cardreading[$key]->imagecard) }}" style="width: 200px;">
								<br><br>
							</a>
						</div>
				    	<div class="col-sm-12 col-md-8 col-md-9 reading-card-wrap">
						    <p class="title1">{{ $read->description }} :</p> <!-- Judul pembacaan -->
						    <p class="title2">{{ $cardreading[$key]->cardname }}</p> <!-- judul kartu -->
						    <p class="title3">{{ $cardreading[$key]->description }}</p> <!-- deskripsi kartu -->
						</div>
				    </div>
				@endforeach

			@else
				bye

			@endif
		</div>
</div>


<script type="text/javascript">
$(document).ready(function() {

	$('.image-popup-no-margins').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
});

</script>
@endsection

@section('footer') 
	@include( 'Site::footer' )
@endsection