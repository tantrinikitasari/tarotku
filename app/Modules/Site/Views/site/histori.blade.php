@extends('layout')

@section('title')Tarotku @endsection

@section('metadata')
	@include( 'Site::metadata' )
@endsection

@section('include') 
	@include( 'Site::include' )
@endsection

@section('navigation') 
	@include( 'Site::navigation' )
@endsection

@section('content')

<div class="choose-outer">
	<div class="container">
		<div class="page-header"> 
			<h2 style="text-align: center">Histori Ramalan</h2>
		</div> 
		<div class="panel panel-lt">
			<div class="panel-heading">
				<i class="icon-history icon-before"></i>Histori Ramalan&nbsp;&nbsp;<small class="italic"></small>
			</div>

			<div class="panel-body bot">
				<div id="apa" class="grid-view">
					<div class="table-responsive">
						<table class="table table-bordered table-striped dataTable">
							<thead>
								<tr>
									<th><span class="badantable" href="#" data-sort="no">No. </span></th>
									<th><span class="badantable" href="#" data-sort="reading_name">Ramalan </span></th>
									<th><span class="badantable" href="#" data-sort="reading.title">Pilihan Ramalan </span></th>
									<th><span class="badantable" href="#" data-sort="reading.title">Jumlah Kartu </span></th>
									<th><span class="badantable" href="#" data-sort="created_at">Tanggal </span></th>
									<th><span class="desc badantable" href="#" data-sort="created_at">Jam </span></th>
								</tr>
							</thead>
							<tbody class="badantable">
							<?php $x=1; ?>
								@foreach($result as $key => $results)
									<tr>
										<td>{{ $x++ }}</td>
										<td><a class="badantable" href="{{ route('historiramalan',[$results->key]) }}"> {{ $results->key }}</a></td>
										<td>{{  $results->name }}</td>
										<td>{{  $results->max }}</td>
										<td>{{ date('d-m-Y', strtotime($results->created_at)) }}</td>
										<td>{{ date('H:i:s', strtotime($results->created_at)) }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<div class="pagination">	
							{!! $result->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('footer') 
	@include( 'Site::footer' )
@endsection