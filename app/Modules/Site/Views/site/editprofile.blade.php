@extends('layout')

@section('title')Tarotku @endsection

@section('metadata')
	@include( 'Site::metadata' )
@endsection

@section('include') 
	@include( 'Site::include' )
@endsection

@section('navigation') 
	@include( 'Site::navigation' )
@endsection

@section('content')

<div class="choose-outer">
	<div class="container">
		<div class="page-header"> 
			<h2 style="text-align: center">Profile</h2>
		</div>
		{!! Form::model($prof,['url' => '/post', 'method' => 'post', 'files'=>true]) !!}
		<!-- {!! Form::open(['route' => ['posteditprof', $prof->id],'method' => 'post']) !!} -->
		<i><h4>{!! Form::label('Upload Profile Picture') !!}</h4></i>
			
			<input type='file' name='pic' id="imgInp"/>
			<img id="pic" name='pic' src="#" alt="your image" style="width:150px;visibility:hidden"/>
       
       	<div class="box-footer">
       		<br>
			{!! Form::submit('Submit',$attributes=array('class'=>'btn')) !!}
		</div>
        
        {!! Form::close() !!}
			
	</div>
</div>
	<script type="text/javascript">
		function readURL(input) {

		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		        	$('#pic').css('visibility','visible')
		            $('#pic').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}
		$("#imgInp").change(function(){
		    readURL(this);
		});
	</script>
@endsection

@section('footer') 
	@include( 'Site::footer' )
@endsection