@extends('layout')

@section('title')Tarotku @endsection

@section('metadata')
	@include( 'Site::metadata' )
@endsection

@section('include') 
	@include( 'Site::include' )
@endsection

@section('navigation') 
	@include( 'Site::navigation' )
@endsection

@section('content')
<div class="choose-outer">
    <div class="container">
        <div class="choose-wrap">
			<nav id="choose-sub-navbar" class="sub-navbar" role="navigation">
			    <div class="container sub-navbar-container">
			        <div class="row sub-navbar-row text-center">
			            <div class="sub-navbar-page-title-wrap col-md-12 col-xs-12">
			                <h1 class="sub-navbar-page-title" style="padding-bottom: 20px">Baca Ramalan Tarotmu Disini</h1>
			            </div> 
			        </div>
			        <div class="row sub-navbar-row">
				        <div class="sub-navbar-utilities-wrap">
			                <!-- <div class="col col-md-4">
			                	<button class="btn btn-default btn-plain btn-card-back btn-sm caret-toggle" type="button" data-toggle="collapse" data-target="#card-backs" aria-expanded="false" aria-controls="change card designs">
			                        Tampilan Kartu <b class="caret"></b>
			                    </button>
			                </div> -->
			                <!-- card back chooser -->
							
			                <div class="row">
			                	<div class="col-md-6">
									<label for="menu" style="float:right; font-size:13px">Pilih Jumlah Kartu</label>
								</div>
								<div class="col-md-6">
									<select class="form-control pilihkartu" id="menu" style="width:80px">
										@foreach ($pilihan as $key => $log)
										<option>
											<a href="#">{{ $log->max }}</a>
										</option>
										@endforeach
									</select>
				                </div>
				               </div>
			            </div>
			        </div>
			        <div class="row"> <!-- card-chooser-collapse -->
			            <div class="collapse card-back-colors text-right" id="card-backs">
			                <a href="javascript:void(0);" color="card-back-1" class="card-back-bg card-back-color-1 active"></a>
			                <a href="javascript:void(0);" color="card-back-2" class="card-back-bg card-back-color-2"></a>			        
			            </div>
			        </div>
			    </div>
			</nav>
			<div class="choose-cards cards-row-bg row card-back-1">
				    <!-- CARD ROWS -->
				    <div class="cards-row-wrap" data-cis="0">
				    	<div class="cards-row-outer">
				    		<div id="cards-row-1" class="cards-row clearfix" data-cisr="0">
			    			 
								@foreach ($cardall as $key => $value) 
			    					<div class="card on" id="card-{{ $value->id }}" onclick="selectCard({{ $value->id }})"></div>
								@endforeach

				    		</div>
				    	</div>            
				    </div>
			</div>

			<div class="instructions">
				<div class="text-center">
					<div class="">
				        <a href="javascript:void(0)" class="btn btn-shuffle">
				            Mengocok kartu <span class="glyphicon glyphicon glyphicon-transfer" aria-hidden="true"></span>
				        </a>
				    </div>
				</div>				            
				<div class="alert alert-warning text-begin text-center text-shadow-minimal clearfix">
				    <i class="icon-chevron-left icon-btn"></i><i class="icon-chevron-left icon-btn"></i>&nbsp;&nbsp;<span>Klik kartu dan pilih <b>0</b> kartu</span>
				</div>
			</div>

			<!-- GUIDE AND NAV -->
			<div class="row nav-guide">
				<!-- Guide -->
				<div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 guide-wrap">
				    <div class="guide nice-box collapse">
				        <p>Pilih <b>0</b> kartu</p>
				    </div>
				</div>
						
				<form id="reading-form" action="{{ route('readingcards') }}" method="post">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<div id="selected_card"></div>

					<!-- Get My Reading BTN -->
					<div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 get-my-reading-wrap">
					    <div class="get-my-reading collapse">
					        <button type="submit" class="btn btn-block btn-lg btn-get-reading">Baca Ramalan Sekarang!
					            <i class="icon-arrow-btn icon-btn"></i>
					        </button>                   
					    </div>
					</div>
				</form>    
			</div>
		</div>
	</div>
</div>

		<a class="back-to-top" href="#" title="Scroll Back to Top" aria-label="Scroll Back to Top">
			<span class="icon-back-to-top"></span>
		</a>


@endsection

@section('footer') 
	@include( 'Site::footer' )
@endsection