@extends('layout')

@section('title')Tarotku @endsection

@section('metadata')
	@include( 'Site::metadata' )
@endsection

@section('include') 
	@include( 'Site::include' )
@endsection

@section('navigation') 
	@include( 'Site::navigation' )
@endsection

@section('content')

<div class="choose-outer">
	<div class="container whitebackground">
		<div class="page-header"> 
			<h2 style="text-align: center">Profile</h2>
		</div>
		<div class="row bottomrow">
			<div class="col-sm-12 col-md-3 col-md-offset-2 ">
				<div class="row"><p class="title1">Gambar Profile</p> </div>
				<div class="row text-center"><img src="{{ asset('assets/profilepic/'.$prof->pic) }}" width="150" height="150"></div>
				<div class="row text-center marginrow"><button class="btn" type="button"><a href="{{ route('editprof') }}">ganti foto</a></button></div>
			</div>
			<div class="col-sm-12 col-md-6">
				<p class="title1">Detail Profile</p>
				<p class="title2">Full name : {{ $prof->fullname }}</p>
				<p class="title2">Birthday : {{  date('d-m-Y', strtotime($prof->birthday)) }}</p>
				<p class="title2">Address : {{ $prof->address }}</p>
				<p class="title2">Phone : {{ $prof->phone }}</p>
			</div>
		</div>	
	</div>
</div>

@endsection

@section('footer') 
	@include( 'Site::footer' )
@endsection