<nav class="navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><img class="img.responsive"src="{{asset('assets/image/logo.png')}}"></a>       
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    @if(!Auth::check())
                    <li><a href="{{ url('auth/login') }}">Login</a></li>
                    <li><a href="{{ url('auth/register') }}">Register</a></li>
                    @else
                    <li><a href="{{ url('/profile') }}">Profile</a></li>
                    <li><a href="{{ url('/histori') }}">Histori Ramalan</a></li>
                    <li><a href="{{ url('auth/logout') }}" id="demo" class="confirm-link">Logout</a></li>
                    @endif
                </ul>
            </div>

<script>
    $(document).ready(function() {
        $('.confirm-link').click(function() {
            var r = confirm("Apakah anda yakin untuk keluar ?");
            if (r === true) {
                window.location.href="{{ url('auth/logout') }}";
            } else {
                return false;
            }
        });
    });
</script>
        
        </div>
</nav>
