<?php

Route::group(array('module' => 'Site','namespace' => 'App\Modules\Site\Controllers'), function() {

	Route::get('/', ['as' => 'index', 'uses' => 'SiteController@index']);  
	Route::post('/reading', ['as' => 'readingcards', 'uses' => 'SiteController@reading']);
	Route::get('/profile', ['as' => 'profiles', 'uses' => 'SiteController@profile']);
	Route::get('/histori', ['as' => 'historis', 'uses' => 'SiteController@histori']);
	Route::get('/histori/ramalan/{key}', ['as' => 'historiramalan', 'uses' => 'SiteController@historiramalan']);
	Route::get('/editprofile', ['as' => 'editprof', 'uses' => 'SiteController@editprofile']);
	Route::get('/post', ['as' => 'posteditprof', 'uses' => 'SiteController@editprofile']);
	Route::post('/post', ['as' => 'save', 'uses' => 'SiteController@saveprofile']);

});	