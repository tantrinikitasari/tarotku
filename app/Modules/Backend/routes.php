<?php

Route::group(array('module' => 'Backend', 'namespace' => 'App\Modules\Backend\Controllers'), function() {

    //Route::resource('Backend', 'BackendController');
    Route::get('/admin', 'BackendController@index');

    Route::model('deck_card', 'DeckCard');
    Route::bind('deck_card', function($value, $route){
    	return App\Models\DeckCard::whereId($value)->firstOrFail();
    });
    Route::resource('/admin/deck_card', 'DeckCardController');

    Route::model('reading', 'Reading');
    Route::bind('reading', function($value, $route){
        return App\Models\Reading::whereId($value)->firstOrFail();
    });
    Route::resource('/admin/reading', 'ReadingController');

    Route::model('user_profile', 'User');
    Route::bind('user_profile', function($value, $route){
    	return App\Models\User::whereId($value)->firstOrFail();
    });
    Route::resource('/admin/users', 'UserProfileController');

});	