@extends('layout_backend.layout')

@section('header')
@stop

@section('footer')

<script type="text/javascript">
	$(document).ready(function(){
		$("#dataTables-example").DataTable();
	});
</script>
@stop

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Deck Card</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">
					{{ Session::get('success') }}
				</div>
			@endif

			@if(Session::has('errors'))
				<div class="alert alert-errors" role="alert">
					{{ Session::get('errors') }}
				</div>
			@endif

			<div class="dataTable_wrapper">
				<table class="table table-striped table-responsive table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Image</th>
							<th>Card Name</th>
							<th>Description</th>
							<th>Manage</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; ?>
						@foreach($deck_cards as $deck_card)
						<tr>
							<td>{{ $i++ }}</td>
							<td><img src="{{ asset('assets/card/'.$deck_card->imagecard) }}" width="80" height="125"></td>
							<td>{{ $deck_card->cardname }}</td>
							<td>{{ $deck_card->description }}</td>
							<td align="center">
								{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('admin.deck_card.destroy', $deck_card->id))) !!}
								<a href="{{ URL::route('admin.deck_card.edit', $deck_card->id) }}" class="btn btn-success btn-circle"><i class="fa fa-pencil"></i></a>						
								
								{!! Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-circle', 'onclick' => 'return confirm("Are you sure remove this card ?")')) !!}
								
								{!! Form::close() !!}
							</td>
						</tr>
						@endforeach	
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop