@extends('layout_backend.layout')

@section('header')
@stop

@section('footer')

<script type="text/javascript">
	$(document).ready(function(){
		$('.product-picture-remove').click(function(){
			var $this = $(this);
			var placeholder = $this.parent().parent().parent();

			placeholder.removeClass('has-image');
			placeholder.find('.image-shown').remove();
			placeholder.find('.image-upload-input').show();

			$(":input[name=keep_image_1]").val("");

			return false;
		});
	});
</script>

@stop

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ ($state == 'add') ? 'Create' : 'Edit' }} Deck Card</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			@if(Session::has('success'))
			<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif

			@if($state == 'add')
			{!! Form::model(new App\Models\DeckCard, ['route' => ['admin.deck_card.store'], 'files' => true]) !!}
			@else
			{!! Form::model($deck_card, ['method' => 'PATCH', 'files' => true, 'route' => ['admin.deck_card.update', $deck_card->id]]) !!}
			@endif
			<div class="form-group{{ $errors->has('imagecard') ? ' has-error' : '' }}" id="div_img">
				<label for="image">Image</label>
				<div style="margin:10px; background:#f9f9f9;height=100px;" class="product-image-placeholder{{ ($deck_card->imagecard != '') ? ' has-image' : '' }}">
					<?php if($deck_card->imagecard != ''): ?>
						<div class="image-shown">
							<div style="position:absolute;left:182px">
								<a href="#" class="btn btn-circle product-picture-remove"><i class="fa fa-trash-o"></i></a>
							</div>
							<span style="display:inline-block;height:100%;vertical-aligh:middle;"></span>
							<img src="{{ asset('assets/card/'.$deck_card->imagecard) }}" style="max-width:170px;max-height:100px;vertical-align:middle;">
							<input type="hidden" name="keep_image_1" value="{{ $deck_card->imagecard }}">		
						</div>
					<?php endif; ?>
					<div class="image-upload-input" style="{{ ($deck_card->imagecard != '') ? 'display:none;' : '' }}">
						<div>&nbsp;</div>
						<div>&nbsp;</div>
						<div>&nbsp;</div>
						<input type="file" name="imagecard">
					</div>	
				</div>
				<?php echo $errors->first('imagecard', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group{{ $errors->has('cardname') ? ' has-error' : '' }}">
				<label for="cardname">Cardname</label>
				{!! Form::text('cardname', null, array('class' => 'form-control')) !!}
				<?php echo $errors->first('cardname', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
				<label for="description">Description</label>
				{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
				<?php echo $errors->first('description', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group" style="margin-top:30px;">
				{!! Form::button('<i class="fa fa-save fa-fw"></i>Save', ["type" => "submit", "class" => "btn btn-primary btn-block"]) !!}
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@stop