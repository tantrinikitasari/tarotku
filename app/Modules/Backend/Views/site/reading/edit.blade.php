@extends('layout_backend.layout')

@section('header')
@stop

@section('footer')
@stop

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ ($state == 'add') ? 'Create' : 'Edit' }} User</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			@if(Session::has('success'))
			<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif

			@if($state == 'add')
			{!! Form::model(new App\Models\User, ['route' => ['admin.users.store']]) !!}
			@else
			{!! Form::model($user, ['method' => 'PATCH', 'route' => ['admin.users.update', $user->id]]) !!}
			@endif
			
			<div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
				<label for="fullname">Full Name</label>					
				{!! Form::text('fullname', null, array('class' => 'form-control')) !!}
				<?php echo $errors->first('fullname', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
				<label for="birthday">Birthday</label>
				{{ Form::text('date', '', array('birthday' => 'datepicker') }}
				<?php echo $errors->first('birthday', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
				<label for="address">Address</label>
				{!! Form::text('address', null, array('class' => 'form-control')) !!}
				<?php echo $errors->first('address', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
				<label for="phone">Phone</label>
				{!! Form::text('phone', null, array('class' => 'form-control')) !!}
				<?php echo $errors->first('phone', '<span class="help-block">:message</span>'); ?>
			</div>

			<div class="form-group" style="margin-top:30px;">
				{!! Form::button('<i class="fa fa-save fa-fw"></i>Save', ["type" => "submit", "class" => "btn btn-primary btn-block"]) !!}
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@stop