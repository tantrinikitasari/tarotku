@extends('layout_backend.layout')

@section('header')
@stop

@section('footer')

<script type="text/javascript">
	$(document).ready(function(){
		$("#dataTables-example").DataTable();
	});
</script>
@stop

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">User Profile</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">
					{{ Session::get('success') }}
				</div>
			@endif

			@if(Session::has('errors'))
				<div class="alert alert-errors" role="alert">
					{{ Session::get('errors') }}
				</div>
			@endif

			<div class="dataTable_wrapper">
				<table class="table table-striped table-responsive table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Full Name</th>
							<th>Birthday</th>
							<th>Address</th>
							<th>Phone</th>
							<th>Manage</th>
						</tr>
					</thead>
					<tbody>
					<?php $i=1; ?>
						@foreach($user as $user)
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{ $user->fullname}}</td>
							<td>{{ $user->birthday }}</td>
							<td>{{ $user->address }}</td>
							<td>{{ $user->phone }}</td>
							<td align="center">
								{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('admin.users.destroy', $user->id))) !!}
								<a href="{{ URL::route('admin.users.edit', $user->id) }}" class="btn btn-success btn-circle"><i class="fa fa-pencil"></i></a>						
								
								{!! Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-circle', 'onclick' => 'return confirm("Are you sure remove this card ?")')) !!}
								
								{!! Form::close() !!}
							</td>
						</tr>
						@endforeach	
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop