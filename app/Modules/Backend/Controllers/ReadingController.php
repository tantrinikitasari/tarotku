<?php

namespace App\Modules\Backend\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;

use App\Models\Reading;

class ReadingController extends Controller
{
    private $rules = array('category_id' => 'required|numeric', 'description' => 'required');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $readings = Reading::orderBy('category_id', 'asc')->get();
        return view('Backend::site.reading.index', compact('readings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state = "add";
        $reading = new Reading;
        return view('Backend::site.reading.edit', compact('state', 'reading'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $reading                      = new Reading;
        $reading->category_id         = $request->category_id;
        $reading->no                  = $request->no;
        $reading->description         = $request->description;
        $reading->save();

        return redirect('/admin/reading/create')->with('success', 'Reading has been added.');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Reading $reading)
    {
        $state = 'edit';
        return view('Backend::site.reading.edit', compact('state', 'reading'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reading $reading)
    {
        $this->validate($request, $this->rules);
        
        $reading->category_id         = $request->category_id;
        $reading->no                  = $request->no;
        $reading->description         = $request->description;
        $reading->update();

        return redirect('/admin/reading/'.$reading->id.'/edit')->with('success', 'Reading has been updated.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reading $reading)
    {

        $reading->delete();

        return back()->with('success', 'Reading has been remove');
    }
}
