<?php

namespace App\Modules\Backend\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\DeckCard;
use Illuminate\Filesystem\Filesystem;

class DeckCardController extends Controller
{

    private $rules = array('cardname' => 'required', 'imagecard' => 'required|mimes:jpeg,png|max:2000', 'description' => 'required');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deck_cards = DeckCard::orderBy('created_at', 'desc')->get();
        return view('Backend::site.deck_card.index', compact('deck_cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state = "add";
        $deck_card = new DeckCard;
        return view('Backend::site.deck_card.edit', compact('state', 'deck_card'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $deck_card                      = new DeckCard;
        $deck_card->cardname            = $request->cardname;
        $deck_card->description         = $request->description;
        $deck_card->save();

        if($request->hasFile('imagecard')){
            $file = $request->file('imagecard');
            $filename = str_replace(array('/', '\\', '..'), '', $file->getClientOriginalName());
            $extension = substr($filename, strrpos($filename, '.') + 1);

            $filename = sprintf('%d_%s.%s', $deck_card->id, str_random(5), $extension);
            $file->move('assets/card/', $filename);

            $deck_card->imagecard = $filename;
            $deck_card->update();
        }

        return redirect('/admin/deck_card/create')->with('success', 'Card has been added.');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DeckCard $deck_card)
    {
        $state = 'edit';
        return view('Backend::site.deck_card.edit', compact('state', 'deck_card'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeckCard $deck_card)
    {
        if($request->keep_image_1 != '')
            unset($this->rules['imagecard']);

        $this->validate($request, $this->rules);

        $deck_card->cardname            = $request->cardname;
        $deck_card->description         = $request->description;

        if($request->hasFile('imagecard')){
            $file = $request->file('imagecard');
            if($file != null){

                $filename = str_replace(array('/', '\\', '..'), '', $file->getClientOriginalName());
                $extension = substr($filename, strrpos($filename, '.') + 1);

                $filename = sprintf('%d_%s.%s', $deck_card->id, str_random(5), $extension);
                $file->move('assets/card/', $filename);

                $path = 'assets/card/';
                $file = $deck_card->imagecard;

                $files = new Filesystem;
                $files->delete($path.$file);

                $deck_card->imagecard = $filename;
            }
        }

        $deck_card->update();
        return redirect('/admin/deck_card/'.$deck_card->id.'/edit')->with('success', 'Card has been updated.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeckCard $deck_card)
    {
        $path = 'assets/card/';
        $file = $deck_card->imagecard;

        $files = new Filesystem;
        $files->delete($path.$file);

        $deck_card->delete();

        return back()->with('success', 'Card has been remove');
    }
}
