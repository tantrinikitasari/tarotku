<?php

namespace App\Modules\Backend\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;

use App\Models\User;

class UserProfileController extends Controller
{
    private $rules = array('fullname' => 'required', 'birthday' => 'required', 'address' => 'required', 'phone' => 'required|numeric');
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::orderBy('id', 'asc')->get();
        return view('Backend::site.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state = "add";
        $user = new User;
        return view('Backend::site.user.edit', compact('state', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $user                   = new User;
        $user->fullname         = $request->fullname;
        $user->birthday         = $request->birthday;
        $user->address          = $request->address;
        $user->phone            = $request->phone;
        $user->save();

        return redirect('/admin/user/create')->with('success', 'User has been added.');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $state = 'edit';
        return view('Backend::site.user.edit', compact('state', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, $this->rules);
        
        $user->fullname         = $request->fullname;
        $user->birthday         = $request->birthday;
        $user->address          = $request->address;
        $user->phone            = $request->phone;
        $user->save();

        return redirect('/admin/user/'.$user->id.'/edit')->with('success', 'User has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
