<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('success', function () {
    return view('nextregister');
});


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('auth/nextregister', 'Auth\AuthNextController@getnextRegister');
Route::post('auth/nextregister', 'Auth\AuthNextController@postnextRegister');

Route::get('register', ['as' => 'register', 'uses' => 'CheckController@register' ]);
Route::post('/register', ['as' => 'post-registration', 'uses' =>   'CheckController@doRegister']);
Route::controllers(['password' => 'Auth\PasswordController',]);



// Route::group(array('module' => 'Site','prefix' => 'site', 'as' => 'site.', 'namespace' => 'App\Modules\Site\Controllers'), function() {

// 	Route::post('/profile', ['as' => 'profile', 'uses' => 'SiteController@profile']);
// 	Route::post('/histori', ['as' => 'histori', 'uses' => 'SiteController@profile']);


// });	


//Route::get('sample/{name}', 'CheckController@index');

//Cara tes Models
//-------------------------------------------
// use App\Models\ResultReading;
// Route::get('tes', function () {
// 	$cat = ResultReading::get();
// 	echo '<pre>';
// 	// print_r ($cat);
// 		foreach ($cat as $key => $value) {
// 			print_r($value->results);
// 		}
// 	echo '</pre>';
// });