<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultReadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_reading', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('result_id')->unsigned()->index();
            $table->foreign('result_id')->references('id')->on('result')->onDelete('cascade');
            $table->integer('reading_id')->unsigned()->index();
            $table->foreign('reading_id')->references('id')->on('reading')->onDelete('cascade');
            $table->integer('deck_id')->unsigned()->index();
            $table->foreign('deck_id')->references('id')->on('deck_card')->onDelete('cascade');
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();  
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('result_reading');

    }
}
