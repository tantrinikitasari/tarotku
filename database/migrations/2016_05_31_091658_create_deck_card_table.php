<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeckCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deck_card', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cardname');
            $table->text('description');
            $table->text('imagecard');
            $table->tinyInteger('created_by')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
            });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deck_card');
    }
}
