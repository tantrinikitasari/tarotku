<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat3 = DB::table('category')->insertGetId([
            'name' => 'Past-Present-Future',
            'max' => 3,
            'status' => 0,
            'created_by' => 1,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_by' => 1,
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

		$cat6 = DB::table('category')->insertGetId([
            'name' => 'Universal Lotus',
            'max' => 6,
            'status' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_by' => 1,
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

		$cat10 = DB::table('category')->insertGetId([
            'name' => 'The Celtic Cross',
            'max' => 10,
            'status' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_by' => 1,
            'updated_at' => date('Y-m-d h:i:s'),
        ]);


        if($cat3) {

         	DB::table('reading')->insert([
	         	'category_id' => $cat3,
	            'no' => 1,
	            'description' => 'Keadaan kamu di masa lalu',
	            'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	

        	DB::table('reading')->insert([
	         	'category_id' => $cat3,
	            'no' => 2,
	            'description' => 'Keadaan kamu sekarang',
	            'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('reading')->insert([
	         	'category_id' => $cat3,
	            'no' => 3,
	            'description' => 'Keadaan kamu di masa yang akan datang',
	            'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);
         }
		
		if($cat6) {
         	DB::table('reading')->insert([
	         	'category_id' => $cat6,
	            'no' => 1,
	            'description' => 'Keadaan kamu sekarang',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	

         	DB::table('reading')->insert([
	         	'category_id' => $cat6,
	            'no' => 2,
	            'description' => 'Keinginan kamu saat ini',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	

         	DB::table('reading')->insert([
	         	'category_id' => $cat6,
	            'no' => 3,
	            'description' => 'Ketakutanmu',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	

         	DB::table('reading')->insert([
	         	'category_id' => $cat6,
	            'no' => 4,
	            'description' => 'Yang akan terjadi pada kamu',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	

         	DB::table('reading')->insert([
	         	'category_id' => $cat6,
	            'no' => 5,
	            'description' => 'Yang akan melawanmu',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	

         	DB::table('reading')->insert([
	         	'category_id' => $cat6,
	            'no' => 6,
	            'description' => 'Keadaan kamu di masa datang atau tujuan',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

		}

		if ($cat10) {
			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 1,
	            'description' => 'Keadaan kamu sekarang : orang sekitar dan faktor-faktor yang mempengaruhi keadaan sekarang',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 2,
	            'description' => 'Yang akan menjadi hambatan kamu : apa yang akan menentang dan apa yang harus diwaspadai oleh kamu',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 3,
	            'description' => 'Awal permasalahan yang menjadi penyebab keadaan kamu sekarang',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 4,
	            'description' => 'Keadaan di masa lalu yang berkaitan dengan keadaan kamu sekarang',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 5,
	            'description' => 'Kemungkinan keadaan jika terjadi perubahan situasi',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 6,
	            'description' => 'Kemungkinan keadaan jika tidak terjadi perubahan situasi',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 7,
	            'description' => 'Bagaimana kamu menilai dirimu sendiri',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 8,
	            'description' => 'Pengaruh orang di sekitar kamu (keluarga, teman, pasangan atau partner kerja)',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 9,
	            'description' => 'Yang menjadi harapan dan ketakutanmu',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

			DB::table('reading')->insert([
	         	'category_id' => $cat10,
	            'no' => 10,
	            'description' => 'Tujuan baru : apa yang bisa kamu lakukan selanjutnya',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);		    	

		}    

    }
}
