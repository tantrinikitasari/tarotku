<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('users')->insertGetId([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('asdf'),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

        $id2 = DB::table('users')->insertGetId([
            'username' => 'ainun',
            'email' => 'ainun@gmail.com',
            'password' => bcrypt('cahyono'),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);



         if($id){

            DB::table('user_profile')->insert([
                'user_id' => $id,
                'fullname' => 'administrator',
                'birthday' => date('Y-m-d', strtotime('1992-01-26')),
                'address' => 'jakarta pusat',
                'phone' => '081448811190',
                'gender' => 'laki-laki',
                'pic' => 'ava-laki.png',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s'),
            ]); 
        }
         if($id2){

         	DB::table('user_profile')->insert([
	         	'user_id' => $id,
	            'fullname' => 'Siti Ainun',
	            'birthday' => date('Y-m-d', strtotime('1992-05-21')),
	            'address' => 'jakarta selatan',
                'phone' => '08167990001',
                'gender' => 'perempuan',
	            'pic' => 'ava.png',
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);	
        }
         
    }
}
