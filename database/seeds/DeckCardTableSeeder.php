<?php

use Illuminate\Database\Seeder;

class DeckCardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = DB::table('deck_card')->get();
        if ( count($check) == 0 ) {

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Fool',
	            'description' => 'Kartu The Fool ini diartikan tidak positif atau negatif namun berkemungkinan baik. Kini Kamu sedang dalam perjalanan atau Kamu sedang memulai suatu hal yang baru. Kalaupun Kamu sedang berada dalam suatu pilihan, sebaiknya gunakan pikiran dan emosi yang  tenang, beri waktu diri Kamu untuk melihat baik dan buruknya suatu pilihan tersebut. Jika pilihan Kamu tepat maka akan berujung baik, namun bila pilihan kurang tepat mungkin Kamu harus mencobanya kembali. Kartu ini keluar, menandakan juga bahwa Kamu orang yang sangat berani dalam mengambil resiko. Dalam pikiran Kamu selalu langkah pertama apa yang harus Kamu ambil tanpa memikirkan resikonya. Karena sesungguhnya Kamu termasuk memiliki kepercayaan diri penuh terhadap diri Kamu sendiri, sehingga apapun yang dikatakan orang lain Kamu tidak akan peduli. ',
	            'imagecard' => '00.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Magician',
	            'description' => 'Magician adalah gabungan dari positif dan negatif. Kartu ini menandakan adanya kemauan dan kekuatan yang besar. Seperti simbol-simbol dalam kartu bahwa ada kekuatan yang tak terbatas yang akan Kamu terima. Sekarang adalah waktu untuk bertindak, jika Kamu tahu apa yang ingin Kamu capai dan mengapa. Karena kekuatan transformasi yang ada mengubah keinginan Kamu ke tujuan, pikiran Kamu ke dalam tindakan, tujuan Kamu ke dalam prestasi. Jika Kamu baru saja bertemu dengan kegagalan, sekarang Kamu dapat mengubah kegagalan itu menjadi sukses semudah Magician perubahan api ke dalam air.',
	            'imagecard' => '01.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The High Priestess',
	            'description' => 'Kartu ini menandakan Kamu untuk lebih peka terhadap apa yang terjadi disekitar Kamu. Terima segala nasehat dari orang-orang terdekat mungkin dapat membantu Kamu menemukan kebijaksanaan. Jika Kamu memiliki sebuah keputusan penting sehingga membuat Pendeta muncul, ini sering merupakan tanda bahwa jawaban akan terungkap kepada Kamu, jika Kamu sabar dan terbuka untuk mendengar suara hati dan intuisi.  Kamu hanya harus menunggu dan bisa menerima pesan batin. Pelajarannya adalah bahwa segala sesuatu yang perlu Kamu ketahui sudah ada dalam diri Kamu.',
	            'imagecard' => '02.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Empress',
	            'description' => 'Ini adalah saat Kamu memahami sisi feminim, untuk mendengarkan intuisi dan emosi yang ada pada diri kita. Bagi Kamu yang ingin menemukan seseorang, kini saatnya Kamu untuk lebih bersinar. Karena lawan jenis akan lebih tertarik dengan Kamu. Kalaupun Kamu sedang dalam sebuah hubungan, hubungan akan lebih manis dan lebih baik. Bagi Kamu yang menginginkan keturunan, ini adalah tanda positif untuk Kamu memulai lagi. Dan soal pekerjaan, orang akan terinspirasi oleh ide-ide Kamu, gairah Kamu, dan cara Kamu melakukan sesuatu. Karena suara hati Kamu saat ini membimbing Kamu dalam bertindak. The Empress mengarahkan kita untuk mengikuti kebahagiaan kita, dan bahwa sisanya akan mengikuti.',
	            'imagecard' => '03.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Emperor',
	            'description' => 'The Emperor menunjukkan bahwa dominasi pikiran daripada hati, terkadang tidak dibutuhkan. Tetapi dalam beberapa kasus berpikir dengan pikiran itu perlu. Ketika pilihan sulit gunakan logika untuk menjaga konsentrasi dan fokus, dan ini adalah sesuatu periode yang akan membuat lebih mudah bagi Kamu untuk melakukan apapun. Selalu fokus dan bawa ketegasan serta keyakinan pada diri Kamu. Lakukan untuk yang terbaik. Pekerjaan yang Kamu lakukan berjalan dengan baik. Jika Kamu sedang mencari cinta, mungkin saat ini Kamu sedang dekat dengan orang yang lebih dewasa. Bukan cinta pada pandangan pertama, namun cinta akan tumbuh jika Kamu juga sabar dengan prosesnya. ',
	            'imagecard' => '04.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Hierophant ',
	            'description' => 'Akan datang seseorang yang layaknya seperti pengajar atau guru bagi Kamu. Seseorang yang memiliki pandangan lebih dari Kamu, yang dapat membuat Kamu yakin terhadap pekerjaan atau apapun yang sedang Kamu kerjakan saat ini. Karena sekarang Kamu sedang dalam pengambilan keputusan. Ambil solusi yang menurut Kamu tepat. Dan jawaban yang tepat itu terdapat dalam diri Kamu sendiri. Jika sedang dalam keadaan stagnan, baiknya lakukan hal-hal yang tradisional, seperti Menonton bersama teman-teman di hari Sabtu, memasak bersama dengan kerabat, atau sekedar berkumpul menikmati kopi di sore hari. Cara-cara tradisional atau lama ini dapat membantu Kamu saat ini.',
	            'imagecard' => '05.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Lovers ',
	            'description' => 'Akan ada cinta yang baru, cinta bersemi kembali, pilihan cinta yang juga dapat menyebabkan penghancuran cinta. Karena kartu ini membawa emosi yang berasal dari berbagai macam sumber. Entah dari Yang Maha Kuasa, pasangan, partner kerja, teman dan lain-lain. Cinta seperti api yang dapat lebih membara,akan tetapi juga dapat menghancurkan jika digunakan sembarangan. Yang sedang memiliki pasangan, kekuatan cinta mulai diuji dengan berbagai konflik namun dengan kebijaksanaan dan kesabaran akan terlewati. Bagi yang sedang dalam suatu pilihan, saatnya untuk memilih seseorang mana yang lebih berarti untuk diri sendiri dan semua pihak yang terlibat. Karena setiap keputusan terdapat konsekwensi yang tidak akan pernah bisa diubah, tidak peduli berapa banyak orang yang tidak setuju dengan Kamu.',
	            'imagecard' => '06.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Chariot',
	            'description' => 'Emosi, gairah, kekuatan yang sangat kuat dan sangat liar. Suatu disiplin diperlukan untuk mengontrol emosi yang liar tersebut. Mengendalikan emosi dan menempatkan emosi di waktu yang tepat jauh lebih bijaksana. Kekuatan Kamu dalam genggaman baiknya segala sesuatu yang masih di angan-angan, segera bertindak. Karena disini semua cita-cita akan terwujud. Kemenangan akan didapat melalui disiplin dan percaya diri, saat di mana semua kebohongan akan kalah. Besar keberhasilan dan prestasi akan datang kepada Kamu jika Kamu menguasai gairah Kamu dan percaya pada kekuatan diri Kamu. Jangan biarkan apapun mengalihkan perhatian dari tujuan Kamu, dan lanjutkan dengan penerbangan langsung seperti panah. Tidak ada yang di luar kemampuan Kamu jika Kamu percaya pada kekuatan Kamu sendiri. Omong kosong!',
	            'imagecard' => '07.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'Strength',
	            'description' => 'Kekuatan menunjukkan pentingnya pikiran menguasai materi, fokus terhadap apa yang Kamu inginkan. Titik utama adalah Kamu memiliki kemampuan untuk memanfaatkan pikiran dan menggunakannya untuk Kamu sendiri. Ditambah Kamu memiliki tanggung jawab dan jangan lelah untuk bersabar. Pencerahan akan datang hanya ketika waktunya tepat; tidak bisa diburu-buru.',
	            'imagecard' => '08.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Hermit',
	            'description' => 'Munculnya Hermit adalah panggilan untuk mempelajari lebih lanjut tentang diri Kamu dan sifat keberadaan Kamu, dan semua orang mengalami panggilan ini di beberapa titik dalam hidup mereka. Ini sebagai tanda bahwa masalah duniawi dapat ditunda dan pekerjaan batin yang harus dilakukan sekarang. Kamu mungkin bahkan sedang berada dalam kerangka pikiran bahwa Kamu perlu waktu sendirian - jangan takut untuk mengambilnya. Bahkan jika itu hanya lima menit berjalan kaki di sekitar rumah sehingga Kamu dapat membersihkan kepala Kamu. Untuk Kamu yang sedang dalam suatu perjalanan "berusaha sangat keras untuk melakukan hal yang benar." Pastikan bahwa "melakukan hal yang benar" meliputi melakukan apa yang tepat untuk Kamu secara pribadi, dan bukan hanya mengkhawatirkan tentang keinginan dan kebutuhan orang lain.',
	            'imagecard' => '09.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Wheel Of Fortune',
	            'description' => 'The Wheel of Fortune memberitahu Kamu secara umum, tampak seolah-olah hal-hal yang berubah. Dalam kebanyakan kasus, ini menunjukkan positif, perubahan yang diperlukan tetapi bagi sebagian orang, perubahan itu sendiri sangat sulit dan hampir dapat traumatis. Siklus kehidupan yang berputar seperti roda. Jika kamu sedang berada diatas, sebaiknya jaga tindakan kamu agar tidak berbalik keadaan. Untuk pasangan mungkin sekarang waktunya bernegosiasi. Untuk masalah finansial besar kemungkinan sekarang keadaan berubah membaik yang sebelumnya mengalami kesulitan. Takdir datang tanpa suatu pertKamu, tetapi sering dapat dilihat datang jika Kamu tahu di mana dan bagaimana untuk mencari mereka.',
	            'imagecard' => '10.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'Justice',
	            'description' => 'Roda kehidupan mengingatkan kita terhadap “Apa yang kita tanam baik, akan berbuah manis” begitu pula sebaliknya. Waktunya karma yang memegang kontrol kehidupan Kamu saat ini. Keadilan akan berpihak pada kita yang benar. Karena apapun yang kamu lakukan pasti akan berbalik kepadamu. Keharmonisan, keseimbangan, keadilan inti dari kartu ini. Kamu tidak mendapatkan apa yang Kamu harapkan, atau bahkan apa yang Kamu inginkan - Kamu mendapatkan apa yang Kamu layak. Jika Kamu layak hal-hal baik maka akan terwujud. Jika Kamu layak mendapatkan hukuman maka diberikan dengan tidak kasih sayang atau ejekan. Kamu hanya mendapatkan kembali apa yang telah Kamu buat sendiri.',
	            'imagecard' => '11.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Hanged Man',
	            'description' => 'Jalan keluar untuk masalah kamu mungkin sederhana, tapi bukan jalan yang terbaik. Jika kamu sedang takut, lepaskan ketakutanmu. Hanged Man mendesak kamu untuk melihat sudut pandang berbeda di dalam situasi yang ada. Dan dalam situasi apapun selalu ada pengorbanan. Ada yang harus dikorbankan untuk mendapat sesuatu yang Kamu inginkan.  ',
	            'imagecard' => '12.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'Death',
	            'description' => 'Kartu ini menunjukkan perubahan atau transformasi pada hidup seseorang. Perubahan ini merujuk pada sesuatu dalam gaya hidup kamu ataupun sikap dan perspektif lama yang tidak lagi berguna. Perubahan ini sedikit dipaksa karena ada sesuatu yang membuat kamu harus merubahnya. Kartu Kematian adalah panggilan bangun Kamu. Kematian bukan hanya kehancuran; itu adalah kehancuran diikuti oleh pembaharuan baru. Meskipun satu pintu mungkin telah ditutup, yang lain akan terbuka. Apakah Kamu memiliki keberanian untuk perubahan? ',
	            'imagecard' => '13.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'Temperance',
	            'description' => 'Temperance adalah kartu tentang keseimbangan, dalam banyak hal, dan hubungan dari segala jenis. Persahabatan, keluarga, kemitraan romantis, dan kemitraan kerja semua membutuhkan perhatian Kamu. Kamu mungkin harus mencoba beberapa pendekatan sebelum Kamu menemukan apa yang benar-benar tepat untuk Kamu. Kebutuhan untuk keseimbangan sering tersirat. Konflik hanya dapat diselesaikan melalui kompromi dan kerja sama. Jika hubungan tampaknya keluar dari keseimbangan dalam cara apapun, Temperance sebagai tanda untuk mulai memperbaiki hal-hal sebelum terlalu jauh keluar dari keseimbangan. Sebelum Kamu dapat mencapai keharmonisan dalam hubungan Kamu dengan orang-orang di sekitar Kamu, Kamu harus berdamai dengan diri sendiri terlebih dahulu.',
	            'imagecard' => '14.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Devil',
	            'description' => 'Sangat penting bagi Kamu untuk mengingat bahwa tidak peduli apapun situasi Kamu, bahwa Kamu selalu memiliki pilihan untuk memilih. Hal pertama, jangan biarkan orang lain memberitahu Kamu bahwa pilihan Kamu terbatas. Kamu dapat membebaskan pembatasan apapun yang menghambat. Jadi lakukan yang menurut Kamu baik dengan sedikit kontrol dari diri Kamu sendiri. Lihat kedalam diri Kamu sendiri bahwa Kamu merupakan pribadi yang dapat melakukan segala hal, tanpa terkekang dan tidak ada siapapun atau apapun bisa mengatur Kamu. Disini Kamu tidak dapat mengendalikan hidup Kamu, yang perlu Kamu lakukan adalah melangkah dengan berani. Tetap berpikir positif dan jangan mudah putus asa. Agar energy positif itu dapat mengubah keadaan Kamu.',
	            'imagecard' => '15.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Tower',
	            'description' => 'The Tower adalah kartu tentang perubahan. Menara ini tidak menakutkan atau menyenangkan sebagai representasi bergambar di sebagian deck. Saatnya melakukan perubahan karena apabila Kamu mempertahankan keadaan sekarang seperti keadaan sebelumnya kemungkinan tidak baik. Maka sebaiknya lepaskan yang menjadi keinginan dengan melihat realita sesungguhnya dan berusaha menyesuaikan dengan keadaan. Kamu dapat menggantinya dengan beberapa mimpi dan tujuan yang lebih realistis dan dapat dicapai jika beberapa angan-angan berantakan kali ini. Bermimpi adalah penting, tetapi juga penting untuk hidup dalam kenyataan juga.',
	            'imagecard' => '16.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Star',
	            'description' => 'Kamu sedang dalam kondisi penuh percaya diri dan berpikir positif yang tinggi. Kali ini pertanda baik untukmu, pertanda adanya harapan untuk sampai pada tujuanmu. Jika kamu sedang merancang sesuatu, sekarang adalah saatnya kamu bisa melakukan segala hal. Peluang baru akan terbuka untuk kamu. Saat yang baik untuk menjalin hubungan baru dengan orang baru. Akan berkecukupan materi, apapun yang kamu butuhkan akan terpenuhi lebih banyak. Kamu juga akan menemukan jalan keluar, seperti sinar bintang di malam hari. Menerangi setiap orang yang sedang dalam kegelapan.',
	            'imagecard' => '17.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Moon',
	            'description' => 'Bulan mengartikan bahwa kamu harus waspada terhadap siapapun dan keadaan. Karena tidak semua yang terlihat, sesuai dengan apa yang kita pikirkan. Yang dapat kamu andalkan adalah kekuatan insting dan batin. Karena disini kamu tidak mendapat pertolongan dalam perjalanan panjangmu. Kamu dibiarkan untuk melakukan perjalanan dan mengambil pelajarannya nanti. Dan perjalanan ini kamu lakukan tanpa petunjuk dan bantuan apapun. Segala sesuatu dapat berubah secara mendadak.',
	            'imagecard' => '18.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The Sun',
	            'description' => 'Penebus, pembawa kedamaian dan kabar baik setelah adanya permasalahan. Perang telah berakhir dan akan ada jalan untuk perdamaian; kebencian digantikan oleh cinta; takut diatasi dengan keberanian. Ini benar-benar waktu untuk merayakan! Kekuatan baik menang diatas kejahatan , dan ketika itu terjadi kamu patut senang karena sedikit demi sedikit cahaya mulai memasuki kehidupanmu. Tidak ada yang lebih kuat dari Matahari, karena tidak ada kekuatan Bumi yang dapat melawan Matahari. Ini bisa menjadi pemberita sukacita, kebahagiaan, kelahiran seorang anak, keluarga yang stabil, kemakmuran materi atau hampir setiap ujung yang positif. Sukses datang jika Kamu percaya diri dan berani dalam penggunaan energi kreatif Kamu. ',
	            'imagecard' => '19.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'Judgement',
	            'description' => 'Peringatan untuk kamu yang cepat mengambil kesimpulan atau memutuskan sesuatu. Baiknya ambil waktu kamu untuk memikirkan segala sesuatu tindakan jika berhubungan dengan pengambilan keputusan. Masa lalu dan kesalahan yang berada di belakang Kamu, dan Kamu siap untuk memulai kehidupan baru. Kamu bahkan mungkin merasa mendapat panggilan - sebuah keyakinan pribadi dari apa yang Kamu lakukan.',
	            'imagecard' => '20.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        	DB::table('deck_card')->insert([
	         	'cardname' => 'The World',
	            'description' => 'Kartu ini menjadi kartu penyelesaian setelah kerja keras kamu. Kartu Dunia manandai waktu dalam hidup Kamu di mana satu siklus berakhir dan selanjutnya baru saja dimulai. Ini merupakan pencapaian akhir dari semua harapan duniawi dan keinginan kamu. Ini adalah konfirmasi dari keberhasilan dan hadiah untuk semua cobaan dan cobaan. Dengan kedatangan Dunia datang kesuksesan terjamin dan kesejahteraan materi, serta pemenuhan emosional, dan pertumbuhan dalam arti spiritual.',
	            'imagecard' => '21.jpg',
	       		'created_by' => 1,
	            'created_at' => date('Y-m-d h:i:s'),
	            'updated_by' => 1,
	            'updated_at' => date('Y-m-d h:i:s'),
        	]);

        }	
    }
}
