<?php

use Illuminate\Database\Seeder;

class ResultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('result')->insert([
            'key' => str_random(10),
            'user_id' => 1,
            'username' => 'Administrator',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

        // DB::table('result')->insert([
        //     'key' => str_random(10),
        //     'user_id' => 0,               
        //     'username' => 'Guest_18',        
        //     'created_at' => date('Y-m-d h:i:s'),
        //     'updated_at' => date('Y-m-d h:i:s'),
        // ]);
    }
}
