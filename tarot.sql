-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2016 at 05:15 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tarot`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `max`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_at`) VALUES
(1, 'Past-Present-Future', 3, 0, 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(2, 'Universal Lotus', 6, 1, 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(3, 'The Celtic Cross', 10, 1, 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipstrick` text COLLATE utf8_unicode_ci NOT NULL,
  `termcondition` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `counter`
--

CREATE TABLE `counter` (
  `id` int(10) UNSIGNED NOT NULL,
  `no` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `counter`
--

INSERT INTO `counter` (`id`, `no`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 31, NULL, '2016-06-28 03:10:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deck_card`
--

CREATE TABLE `deck_card` (
  `id` int(10) UNSIGNED NOT NULL,
  `cardname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `imagecard` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deck_card`
--

INSERT INTO `deck_card` (`id`, `cardname`, `description`, `imagecard`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_at`) VALUES
(1, 'The Fool', 'Kartu The Fool ini diartikan tidak positif atau negatif namun berkemungkinan baik. Kini Kamu sedang dalam perjalanan atau Kamu sedang memulai suatu hal yang baru. Kalaupun Kamu sedang berada dalam suatu pilihan, sebaiknya gunakan pikiran dan emosi yang  tenang, beri waktu diri Kamu untuk melihat baik dan buruknya suatu pilihan tersebut. Jika pilihan Kamu tepat maka akan berujung baik, namun bila pilihan kurang tepat mungkin Kamu harus mencobanya kembali. Kartu ini keluar, menandakan juga bahwa Kamu orang yang sangat berani dalam mengambil resiko. Dalam pikiran Kamu selalu langkah pertama apa yang harus Kamu ambil tanpa memikirkan resikonya. Karena sesungguhnya Kamu termasuk memiliki kepercayaan diri penuh terhadap diri Kamu sendiri, sehingga apapun yang dikatakan orang lain Kamu tidak akan peduli. ', '00.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(2, 'The Magician', 'Magician adalah gabungan dari positif dan negatif. Kartu ini menandakan adanya kemauan dan kekuatan yang besar. Seperti simbol-simbol dalam kartu bahwa ada kekuatan yang tak terbatas yang akan Kamu terima. Sekarang adalah waktu untuk bertindak, jika Kamu tahu apa yang ingin Kamu capai dan mengapa. Karena kekuatan transformasi yang ada mengubah keinginan Kamu ke tujuan, pikiran Kamu ke dalam tindakan, tujuan Kamu ke dalam prestasi. Jika Kamu baru saja bertemu dengan kegagalan, sekarang Kamu dapat mengubah kegagalan itu menjadi sukses semudah Magician perubahan api ke dalam air.', '01.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(3, 'The High Priestess', 'Kartu ini menandakan Kamu untuk lebih peka terhadap apa yang terjadi disekitar Kamu. Terima segala nasehat dari orang-orang terdekat mungkin dapat membantu Kamu menemukan kebijaksanaan. Jika Kamu memiliki sebuah keputusan penting sehingga membuat Pendeta muncul, ini sering merupakan tanda bahwa jawaban akan terungkap kepada Kamu, jika Kamu sabar dan terbuka untuk mendengar suara hati dan intuisi.  Kamu hanya harus menunggu dan bisa menerima pesan batin. Pelajarannya adalah bahwa segala sesuatu yang perlu Kamu ketahui sudah ada dalam diri Kamu.', '02.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(4, 'The Empress', 'Ini adalah saat Kamu memahami sisi feminim, untuk mendengarkan intuisi dan emosi yang ada pada diri kita. Bagi Kamu yang ingin menemukan seseorang, kini saatnya Kamu untuk lebih bersinar. Karena lawan jenis akan lebih tertarik dengan Kamu. Kalaupun Kamu sedang dalam sebuah hubungan, hubungan akan lebih manis dan lebih baik. Bagi Kamu yang menginginkan keturunan, ini adalah tanda positif untuk Kamu memulai lagi. Dan soal pekerjaan, orang akan terinspirasi oleh ide-ide Kamu, gairah Kamu, dan cara Kamu melakukan sesuatu. Karena suara hati Kamu saat ini membimbing Kamu dalam bertindak. The Empress mengarahkan kita untuk mengikuti kebahagiaan kita, dan bahwa sisanya akan mengikuti.', '03.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(5, 'The Emperor', 'The Emperor menunjukkan bahwa dominasi pikiran daripada hati, terkadang tidak dibutuhkan. Tetapi dalam beberapa kasus berpikir dengan pikiran itu perlu. Ketika pilihan sulit gunakan logika untuk menjaga konsentrasi dan fokus, dan ini adalah sesuatu periode yang akan membuat lebih mudah bagi Kamu untuk melakukan apapun. Selalu fokus dan bawa ketegasan serta keyakinan pada diri Kamu. Lakukan untuk yang terbaik. Pekerjaan yang Kamu lakukan berjalan dengan baik. Jika Kamu sedang mencari cinta, mungkin saat ini Kamu sedang dekat dengan orang yang lebih dewasa. Bukan cinta pada pandangan pertama, namun cinta akan tumbuh jika Kamu juga sabar dengan prosesnya. ', '04.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(6, 'The Hierophant ', 'Akan datang seseorang yang layaknya seperti pengajar atau guru bagi Kamu. Seseorang yang memiliki pandangan lebih dari Kamu, yang dapat membuat Kamu yakin terhadap pekerjaan atau apapun yang sedang Kamu kerjakan saat ini. Karena sekarang Kamu sedang dalam pengambilan keputusan. Ambil solusi yang menurut Kamu tepat. Dan jawaban yang tepat itu terdapat dalam diri Kamu sendiri. Jika sedang dalam keadaan stagnan, baiknya lakukan hal-hal yang tradisional, seperti Menonton bersama teman-teman di hari Sabtu, memasak bersama dengan kerabat, atau sekedar berkumpul menikmati kopi di sore hari. Cara-cara tradisional atau lama ini dapat membantu Kamu saat ini.', '05.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(7, 'The Lovers ', 'Akan ada cinta yang baru, cinta bersemi kembali, pilihan cinta yang juga dapat menyebabkan penghancuran cinta. Karena kartu ini membawa emosi yang berasal dari berbagai macam sumber. Entah dari Yang Maha Kuasa, pasangan, partner kerja, teman dan lain-lain. Cinta seperti api yang dapat lebih membara,akan tetapi juga dapat menghancurkan jika digunakan sembarangan. Yang sedang memiliki pasangan, kekuatan cinta mulai diuji dengan berbagai konflik namun dengan kebijaksanaan dan kesabaran akan terlewati. Bagi yang sedang dalam suatu pilihan, saatnya untuk memilih seseorang mana yang lebih berarti untuk diri sendiri dan semua pihak yang terlibat. Karena setiap keputusan terdapat konsekwensi yang tidak akan pernah bisa diubah, tidak peduli berapa banyak orang yang tidak setuju dengan Kamu.', '06.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(8, 'The Chariot', 'Emosi, gairah, kekuatan yang sangat kuat dan sangat liar. Suatu disiplin diperlukan untuk mengontrol emosi yang liar tersebut. Mengendalikan emosi dan menempatkan emosi di waktu yang tepat jauh lebih bijaksana. Kekuatan Kamu dalam genggaman baiknya segala sesuatu yang masih di angan-angan, segera bertindak. Karena disini semua cita-cita akan terwujud. Kemenangan akan didapat melalui disiplin dan percaya diri, saat di mana semua kebohongan akan kalah. Besar keberhasilan dan prestasi akan datang kepada Kamu jika Kamu menguasai gairah Kamu dan percaya pada kekuatan diri Kamu. Jangan biarkan apapun mengalihkan perhatian dari tujuan Kamu, dan lanjutkan dengan penerbangan langsung seperti panah. Tidak ada yang di luar kemampuan Kamu jika Kamu percaya pada kekuatan Kamu sendiri. Omong kosong!', '07.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(9, 'Strength', 'Kekuatan menunjukkan pentingnya pikiran menguasai materi, fokus terhadap apa yang Kamu inginkan. Titik utama adalah Kamu memiliki kemampuan untuk memanfaatkan pikiran dan menggunakannya untuk Kamu sendiri. Ditambah Kamu memiliki tanggung jawab dan jangan lelah untuk bersabar. Pencerahan akan datang hanya ketika waktunya tepat; tidak bisa diburu-buru.', '08.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(10, 'The Hermit', 'Munculnya Hermit adalah panggilan untuk mempelajari lebih lanjut tentang diri Kamu dan sifat keberadaan Kamu, dan semua orang mengalami panggilan ini di beberapa titik dalam hidup mereka. Ini sebagai tanda bahwa masalah duniawi dapat ditunda dan pekerjaan batin yang harus dilakukan sekarang. Kamu mungkin bahkan sedang berada dalam kerangka pikiran bahwa Kamu perlu waktu sendirian - jangan takut untuk mengambilnya. Bahkan jika itu hanya lima menit berjalan kaki di sekitar rumah sehingga Kamu dapat membersihkan kepala Kamu. Untuk Kamu yang sedang dalam suatu perjalanan "berusaha sangat keras untuk melakukan hal yang benar." Pastikan bahwa "melakukan hal yang benar" meliputi melakukan apa yang tepat untuk Kamu secara pribadi, dan bukan hanya mengkhawatirkan tentang keinginan dan kebutuhan orang lain.', '09.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(11, 'The Wheel Of Fortune', 'The Wheel of Fortune memberitahu Kamu secara umum, tampak seolah-olah hal-hal yang berubah. Dalam kebanyakan kasus, ini menunjukkan positif, perubahan yang diperlukan tetapi bagi sebagian orang, perubahan itu sendiri sangat sulit dan hampir dapat traumatis. Siklus kehidupan yang berputar seperti roda. Jika kamu sedang berada diatas, sebaiknya jaga tindakan kamu agar tidak berbalik keadaan. Untuk pasangan mungkin sekarang waktunya bernegosiasi. Untuk masalah finansial besar kemungkinan sekarang keadaan berubah membaik yang sebelumnya mengalami kesulitan. Takdir datang tanpa suatu pertKamu, tetapi sering dapat dilihat datang jika Kamu tahu di mana dan bagaimana untuk mencari mereka.', '10.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(12, 'Justice', 'Roda kehidupan mengingatkan kita terhadap “Apa yang kita tanam baik, akan berbuah manis” begitu pula sebaliknya. Waktunya karma yang memegang kontrol kehidupan Kamu saat ini. Keadilan akan berpihak pada kita yang benar. Karena apapun yang kamu lakukan pasti akan berbalik kepadamu. Keharmonisan, keseimbangan, keadilan inti dari kartu ini. Kamu tidak mendapatkan apa yang Kamu harapkan, atau bahkan apa yang Kamu inginkan - Kamu mendapatkan apa yang Kamu layak. Jika Kamu layak hal-hal baik maka akan terwujud. Jika Kamu layak mendapatkan hukuman maka diberikan dengan tidak kasih sayang atau ejekan. Kamu hanya mendapatkan kembali apa yang telah Kamu buat sendiri.', '11.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(13, 'The Hanged Man', 'Jalan keluar untuk masalah kamu mungkin sederhana, tapi bukan jalan yang terbaik. Jika kamu sedang takut, lepaskan ketakutanmu. Hanged Man mendesak kamu untuk melihat sudut pandang berbeda di dalam situasi yang ada. Dan dalam situasi apapun selalu ada pengorbanan. Ada yang harus dikorbankan untuk mendapat sesuatu yang Kamu inginkan.  ', '12.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(14, 'Death', 'Kartu ini menunjukkan perubahan atau transformasi pada hidup seseorang. Perubahan ini merujuk pada sesuatu dalam gaya hidup kamu ataupun sikap dan perspektif lama yang tidak lagi berguna. Perubahan ini sedikit dipaksa karena ada sesuatu yang membuat kamu harus merubahnya. Kartu Kematian adalah panggilan bangun Kamu. Kematian bukan hanya kehancuran; itu adalah kehancuran diikuti oleh pembaharuan baru. Meskipun satu pintu mungkin telah ditutup, yang lain akan terbuka. Apakah Kamu memiliki keberanian untuk perubahan? ', '13.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(15, 'Temperance', 'Temperance adalah kartu tentang keseimbangan, dalam banyak hal, dan hubungan dari segala jenis. Persahabatan, keluarga, kemitraan romantis, dan kemitraan kerja semua membutuhkan perhatian Kamu. Kamu mungkin harus mencoba beberapa pendekatan sebelum Kamu menemukan apa yang benar-benar tepat untuk Kamu. Kebutuhan untuk keseimbangan sering tersirat. Konflik hanya dapat diselesaikan melalui kompromi dan kerja sama. Jika hubungan tampaknya keluar dari keseimbangan dalam cara apapun, Temperance sebagai tanda untuk mulai memperbaiki hal-hal sebelum terlalu jauh keluar dari keseimbangan. Sebelum Kamu dapat mencapai keharmonisan dalam hubungan Kamu dengan orang-orang di sekitar Kamu, Kamu harus berdamai dengan diri sendiri terlebih dahulu.', '14.jpg', 1, '2016-06-22 09:56:05', 1, '2016-06-22 09:56:05', NULL),
(16, 'The Devil', 'Sangat penting bagi Kamu untuk mengingat bahwa tidak peduli apapun situasi Kamu, bahwa Kamu selalu memiliki pilihan untuk memilih. Hal pertama, jangan biarkan orang lain memberitahu Kamu bahwa pilihan Kamu terbatas. Kamu dapat membebaskan pembatasan apapun yang menghambat. Jadi lakukan yang menurut Kamu baik dengan sedikit kontrol dari diri Kamu sendiri. Lihat kedalam diri Kamu sendiri bahwa Kamu merupakan pribadi yang dapat melakukan segala hal, tanpa terkekang dan tidak ada siapapun atau apapun bisa mengatur Kamu. Disini Kamu tidak dapat mengendalikan hidup Kamu, yang perlu Kamu lakukan adalah melangkah dengan berani. Tetap berpikir positif dan jangan mudah putus asa. Agar energy positif itu dapat mengubah keadaan Kamu.', '15.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL),
(17, 'The Tower', 'The Tower adalah kartu tentang perubahan. Menara ini tidak menakutkan atau menyenangkan sebagai representasi bergambar di sebagian deck. Saatnya melakukan perubahan karena apabila Kamu mempertahankan keadaan sekarang seperti keadaan sebelumnya kemungkinan tidak baik. Maka sebaiknya lepaskan yang menjadi keinginan dengan melihat realita sesungguhnya dan berusaha menyesuaikan dengan keadaan. Kamu dapat menggantinya dengan beberapa mimpi dan tujuan yang lebih realistis dan dapat dicapai jika beberapa angan-angan berantakan kali ini. Bermimpi adalah penting, tetapi juga penting untuk hidup dalam kenyataan juga.', '16.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL),
(18, 'The Star', 'Kamu sedang dalam kondisi penuh percaya diri dan berpikir positif yang tinggi. Kali ini pertanda baik untukmu, pertanda adanya harapan untuk sampai pada tujuanmu. Jika kamu sedang merancang sesuatu, sekarang adalah saatnya kamu bisa melakukan segala hal. Peluang baru akan terbuka untuk kamu. Saat yang baik untuk menjalin hubungan baru dengan orang baru. Akan berkecukupan materi, apapun yang kamu butuhkan akan terpenuhi lebih banyak. Kamu juga akan menemukan jalan keluar, seperti sinar bintang di malam hari. Menerangi setiap orang yang sedang dalam kegelapan.', '17.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL),
(19, 'The Moon', 'Bulan mengartikan bahwa kamu harus waspada terhadap siapapun dan keadaan. Karena tidak semua yang terlihat, sesuai dengan apa yang kita pikirkan. Yang dapat kamu andalkan adalah kekuatan insting dan batin. Karena disini kamu tidak mendapat pertolongan dalam perjalanan panjangmu. Kamu dibiarkan untuk melakukan perjalanan dan mengambil pelajarannya nanti. Dan perjalanan ini kamu lakukan tanpa petunjuk dan bantuan apapun. Segala sesuatu dapat berubah secara mendadak.', '18.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL),
(20, 'The Sun', 'Penebus, pembawa kedamaian dan kabar baik setelah adanya permasalahan. Perang telah berakhir dan akan ada jalan untuk perdamaian; kebencian digantikan oleh cinta; takut diatasi dengan keberanian. Ini benar-benar waktu untuk merayakan! Kekuatan baik menang diatas kejahatan , dan ketika itu terjadi kamu patut senang karena sedikit demi sedikit cahaya mulai memasuki kehidupanmu. Tidak ada yang lebih kuat dari Matahari, karena tidak ada kekuatan Bumi yang dapat melawan Matahari. Ini bisa menjadi pemberita sukacita, kebahagiaan, kelahiran seorang anak, keluarga yang stabil, kemakmuran materi atau hampir setiap ujung yang positif. Sukses datang jika Kamu percaya diri dan berani dalam penggunaan energi kreatif Kamu. ', '19.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL),
(21, 'Judgement', 'Peringatan untuk kamu yang cepat mengambil kesimpulan atau memutuskan sesuatu. Baiknya ambil waktu kamu untuk memikirkan segala sesuatu tindakan jika berhubungan dengan pengambilan keputusan. Masa lalu dan kesalahan yang berada di belakang Kamu, dan Kamu siap untuk memulai kehidupan baru. Kamu bahkan mungkin merasa mendapat panggilan - sebuah keyakinan pribadi dari apa yang Kamu lakukan.', '20.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL),
(22, 'The World', 'Kartu ini menjadi kartu penyelesaian setelah kerja keras kamu. Kartu Dunia manandai waktu dalam hidup Kamu di mana satu siklus berakhir dan selanjutnya baru saja dimulai. Ini merupakan pencapaian akhir dari semua harapan duniawi dan keinginan kamu. Ini adalah konfirmasi dari keberhasilan dan hadiah untuk semua cobaan dan cobaan. Dengan kedatangan Dunia datang kesuksesan terjamin dan kesejahteraan materi, serta pemenuhan emosional, dan pertumbuhan dalam arti spiritual.', '21.jpg', 1, '2016-06-22 09:56:06', 1, '2016-06-22 09:56:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_01_15_105324_create_roles_table', 1),
('2015_01_15_114412_create_role_user_table', 1),
('2015_01_26_115212_create_permissions_table', 1),
('2015_01_26_115523_create_permission_role_table', 1),
('2015_02_09_132439_create_permission_user_table', 1),
('2016_05_31_081536_create_user_profile_table', 1),
('2016_05_31_091658_create_deck_card_table', 1),
('2016_05_31_092503_create_category_table', 1),
('2016_05_31_093738_create_reading_table', 1),
('2016_05_31_094753_create_result_table', 1),
('2016_05_31_095957_create_result_reading_table', 1),
('2016_05_31_103724_create_configuration_table', 1),
('2016_06_10_083819_create_counter_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reading`
--

CREATE TABLE `reading` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `no` tinyint(4) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reading`
--

INSERT INTO `reading` (`id`, `category_id`, `no`, `description`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Keadaan kamu di masa lalu', 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(2, 1, 2, 'Keadaan kamu sekarang', 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(3, 1, 3, 'Keadaan kamu di masa yang akan datang', 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(4, 2, 1, 'Keadaan kamu sekarang', 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(5, 2, 2, 'Keinginan kamu saat ini', 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(6, 2, 3, 'Ketakutanmu', 1, '2016-06-22 09:56:03', 1, '2016-06-22 09:56:03', NULL),
(7, 2, 4, 'Yang akan terjadi pada kamu', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(8, 2, 5, 'Yang akan melawanmu', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(9, 2, 6, 'Keadaan kamu di masa datang atau tujuan', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(10, 3, 1, 'Keadaan kamu sekarang : orang sekitar dan faktor-faktor yang mempengaruhi keadaan sekarang', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(11, 3, 2, 'Yang akan menjadi hambatan kamu : apa yang akan menentang dan apa yang harus diwaspadai oleh kamu', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(12, 3, 3, 'Awal permasalahan yang menjadi penyebab keadaan kamu sekarang', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(13, 3, 4, 'Keadaan di masa lalu yang berkaitan dengan keadaan kamu sekarang', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(14, 3, 5, 'Kemungkinan keadaan jika terjadi perubahan situasi', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(15, 3, 6, 'Kemungkinan keadaan jika tidak terjadi perubahan situasi', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(16, 3, 7, 'Bagaimana kamu menilai dirimu sendiri', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(17, 3, 8, 'Pengaruh orang di sekitar kamu (keluarga, teman, pasangan atau partner kerja)', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(18, 3, 9, 'Yang menjadi harapan dan ketakutanmu', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL),
(19, 3, 10, 'Tujuan baru : apa yang bisa kamu lakukan selanjutnya', 1, '2016-06-22 09:56:04', 1, '2016-06-22 09:56:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `key`, `user_id`, `username`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '20160623032553mw5D3yMFkv', 0, 'Guest_1', '2016-06-23 03:25:53', '2016-06-23 03:25:53', NULL),
(3, '20160623033127a5xUbA4uIX', 0, 'Guest_2', '2016-06-23 03:31:27', '2016-06-23 03:31:27', NULL),
(4, '20160623033152jmrNRzAffw', 0, 'Guest_3', '2016-06-23 03:31:52', '2016-06-23 03:31:52', NULL),
(5, '20160623033240eTeJcwynrv', 0, 'Guest_4', '2016-06-23 03:32:40', '2016-06-23 03:32:40', NULL),
(6, '20160623033259BeKxnfACld', 0, 'Guest_5', '2016-06-23 03:32:59', '2016-06-23 03:32:59', NULL),
(7, '20160623033459604IBVigTX', 0, 'Guest_6', '2016-06-23 03:34:59', '2016-06-23 03:34:59', NULL),
(8, '201606230344190CU6KFm7x4', 1, 'admin', '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(9, '201606230344239UrzrcM7Vd', 1, 'admin', '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(10, '20160623034427QE9zLenyFW', 1, 'admin', '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(11, '201606230344521hRJNpYqao', 1, 'admin', '2016-06-23 03:44:52', '2016-06-23 03:44:52', NULL),
(12, '20160623034542HtemrpohqW', 1, 'admin', '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(13, '20160623034557RuWj3u8ZlK', 1, 'admin', '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(14, '20160623034610mBBtlh17Jg', 0, 'Guest_7', '2016-06-23 03:46:10', '2016-06-23 03:46:10', NULL),
(15, '20160623034620FGE6pAwWD1', 0, 'Guest_8', '2016-06-23 03:46:20', '2016-06-23 03:46:20', NULL),
(16, '20160623034704fGBLvclhh5', 0, 'Guest_9', '2016-06-23 03:47:04', '2016-06-23 03:47:04', NULL),
(17, '20160623034835VWyq3S3XGa', 0, 'Guest_10', '2016-06-23 03:48:35', '2016-06-23 03:48:35', NULL),
(18, '20160623034845QNE1Cc5JYL', 0, 'Guest_11', '2016-06-23 03:48:45', '2016-06-23 03:48:45', NULL),
(19, '20160623035013er6ZNUWdQO', 0, 'Guest_12', '2016-06-23 03:50:13', '2016-06-23 03:50:13', NULL),
(20, '20160623035022lm20gDFio2', 0, 'Guest_13', '2016-06-23 03:50:22', '2016-06-23 03:50:22', NULL),
(21, '20160623035025p8R1uvDAxc', 0, 'Guest_14', '2016-06-23 03:50:25', '2016-06-23 03:50:25', NULL),
(22, '20160623035033ykNiTYtOM7', 0, 'Guest_15', '2016-06-23 03:50:33', '2016-06-23 03:50:33', NULL),
(23, '201606230350454upKyE5ecr', 0, 'Guest_16', '2016-06-23 03:50:45', '2016-06-23 03:50:45', NULL),
(24, '20160623035112PUZ6SMiTWh', 0, 'Guest_17', '2016-06-23 03:51:12', '2016-06-23 03:51:12', NULL),
(25, '20160623035116KUxhE9FKfb', 0, 'Guest_18', '2016-06-23 03:51:16', '2016-06-23 03:51:16', NULL),
(26, '201606230351234kePEgv949', 0, 'Guest_19', '2016-06-23 03:51:23', '2016-06-23 03:51:23', NULL),
(27, '20160623035148gui7OtiMJR', 0, 'Guest_20', '2016-06-23 03:51:48', '2016-06-23 03:51:48', NULL),
(28, '201606230351574jvyDvQJ4q', 0, 'Guest_21', '2016-06-23 03:51:57', '2016-06-23 03:51:57', NULL),
(29, '20160623035218eSFh14Cqof', 0, 'Guest_22', '2016-06-23 03:52:18', '2016-06-23 03:52:18', NULL),
(30, '20160623035228iQSwRdDyCp', 0, 'Guest_23', '2016-06-23 03:52:28', '2016-06-23 03:52:28', NULL),
(31, '20160623035238wa8hEGMPCT', 0, 'Guest_24', '2016-06-23 03:52:38', '2016-06-23 03:52:38', NULL),
(32, '20160623035245AvRPdrZ4PM', 0, 'Guest_25', '2016-06-23 03:52:45', '2016-06-23 03:52:45', NULL),
(33, '20160623035255k0tAu2ejsl', 0, 'Guest_26', '2016-06-23 03:52:55', '2016-06-23 03:52:55', NULL),
(34, '20160623035304h0k5sH8SHD', 0, 'Guest_27', '2016-06-23 03:53:04', '2016-06-23 03:53:04', NULL),
(35, '20160623035325YyVziCfVGo', 1, 'admin', '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(36, '20160623035333RhDrakgwiz', 1, 'admin', '2016-06-23 03:53:33', '2016-06-23 03:53:33', NULL),
(37, '201606230353495awtKMT4Vd', 1, 'admin', '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(38, '20160623035402BloAGTEejF', 1, 'admin', '2016-06-23 03:54:02', '2016-06-23 03:54:02', NULL),
(39, '20160623035453dyvkUsIVjR', 1, 'admin', '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(40, '20160623043036em7YLRDIBa', 1, 'admin', '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(41, '201606230719182MkptIZkBG', 0, 'Guest_28', '2016-06-23 07:19:18', '2016-06-23 07:19:18', NULL),
(42, '20160623080827nUvRX6BJnQ', 1, 'admin', '2016-06-23 08:08:27', '2016-06-23 08:08:27', NULL),
(43, '201606230825239dNq7ycZR9', 1, 'admin', '2016-06-23 08:25:23', '2016-06-23 08:25:23', NULL),
(44, '20160624075004Ag6JW1qqA7', 2, 'ainun', '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(45, '20160624075052q68Kep1iLw', 2, 'ainun', '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(46, '20160627071556OCVMaFDn6v', 0, 'Guest_29', '2016-06-27 07:15:56', '2016-06-27 07:15:56', NULL),
(47, '201606270716257B5guhiwK5', 1, 'admin', '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(48, '20160627071839JRpUjsIDJN', 1, 'admin', '2016-06-27 07:18:39', '2016-06-27 07:18:39', NULL),
(49, '201606280310039MLF5DJnoS', 0, 'Guest_30', '2016-06-28 03:10:03', '2016-06-28 03:10:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `result_reading`
--

CREATE TABLE `result_reading` (
  `id` int(10) UNSIGNED NOT NULL,
  `result_id` int(10) UNSIGNED NOT NULL,
  `reading_id` int(10) UNSIGNED NOT NULL,
  `deck_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `result_reading`
--

INSERT INTO `result_reading` (`id`, `result_id`, `reading_id`, `deck_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 1, 20, '2016-06-23 03:25:53', '2016-06-23 03:25:53', NULL),
(2, 2, 2, 9, '2016-06-23 03:25:53', '2016-06-23 03:25:53', NULL),
(3, 2, 3, 22, '2016-06-23 03:25:53', '2016-06-23 03:25:53', NULL),
(4, 3, 1, 20, '2016-06-23 03:31:27', '2016-06-23 03:31:27', NULL),
(5, 3, 2, 9, '2016-06-23 03:31:27', '2016-06-23 03:31:27', NULL),
(6, 3, 3, 22, '2016-06-23 03:31:27', '2016-06-23 03:31:27', NULL),
(7, 4, 1, 20, '2016-06-23 03:31:52', '2016-06-23 03:31:52', NULL),
(8, 4, 2, 9, '2016-06-23 03:31:52', '2016-06-23 03:31:52', NULL),
(9, 4, 3, 22, '2016-06-23 03:31:52', '2016-06-23 03:31:52', NULL),
(10, 5, 1, 20, '2016-06-23 03:32:40', '2016-06-23 03:32:40', NULL),
(11, 5, 2, 9, '2016-06-23 03:32:41', '2016-06-23 03:32:41', NULL),
(12, 5, 3, 22, '2016-06-23 03:32:41', '2016-06-23 03:32:41', NULL),
(13, 6, 1, 20, '2016-06-23 03:32:59', '2016-06-23 03:32:59', NULL),
(14, 6, 2, 9, '2016-06-23 03:32:59', '2016-06-23 03:32:59', NULL),
(15, 6, 3, 22, '2016-06-23 03:32:59', '2016-06-23 03:32:59', NULL),
(16, 7, 1, 20, '2016-06-23 03:34:59', '2016-06-23 03:34:59', NULL),
(17, 7, 2, 9, '2016-06-23 03:34:59', '2016-06-23 03:34:59', NULL),
(18, 7, 3, 22, '2016-06-23 03:34:59', '2016-06-23 03:34:59', NULL),
(19, 8, 4, 14, '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(20, 8, 5, 13, '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(21, 8, 6, 18, '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(22, 8, 7, 5, '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(23, 8, 8, 19, '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(24, 8, 9, 20, '2016-06-23 03:44:19', '2016-06-23 03:44:19', NULL),
(25, 9, 4, 14, '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(26, 9, 5, 13, '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(27, 9, 6, 18, '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(28, 9, 7, 5, '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(29, 9, 8, 19, '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(30, 9, 9, 20, '2016-06-23 03:44:23', '2016-06-23 03:44:23', NULL),
(31, 10, 4, 14, '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(32, 10, 5, 13, '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(33, 10, 6, 18, '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(34, 10, 7, 5, '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(35, 10, 8, 19, '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(36, 10, 9, 20, '2016-06-23 03:44:27', '2016-06-23 03:44:27', NULL),
(37, 11, 4, 14, '2016-06-23 03:44:52', '2016-06-23 03:44:52', NULL),
(38, 11, 5, 13, '2016-06-23 03:44:52', '2016-06-23 03:44:52', NULL),
(39, 11, 6, 18, '2016-06-23 03:44:52', '2016-06-23 03:44:52', NULL),
(40, 11, 7, 5, '2016-06-23 03:44:53', '2016-06-23 03:44:53', NULL),
(41, 11, 8, 19, '2016-06-23 03:44:53', '2016-06-23 03:44:53', NULL),
(42, 11, 9, 20, '2016-06-23 03:44:53', '2016-06-23 03:44:53', NULL),
(43, 12, 4, 14, '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(44, 12, 5, 13, '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(45, 12, 6, 18, '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(46, 12, 7, 5, '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(47, 12, 8, 19, '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(48, 12, 9, 20, '2016-06-23 03:45:42', '2016-06-23 03:45:42', NULL),
(49, 13, 4, 6, '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(50, 13, 5, 21, '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(51, 13, 6, 17, '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(52, 13, 7, 15, '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(53, 13, 8, 11, '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(54, 13, 9, 2, '2016-06-23 03:45:57', '2016-06-23 03:45:57', NULL),
(55, 14, 1, 13, '2016-06-23 03:46:10', '2016-06-23 03:46:10', NULL),
(56, 14, 2, 19, '2016-06-23 03:46:10', '2016-06-23 03:46:10', NULL),
(57, 14, 3, 5, '2016-06-23 03:46:11', '2016-06-23 03:46:11', NULL),
(58, 15, 1, 13, '2016-06-23 03:46:20', '2016-06-23 03:46:20', NULL),
(59, 15, 2, 19, '2016-06-23 03:46:20', '2016-06-23 03:46:20', NULL),
(60, 15, 3, 5, '2016-06-23 03:46:21', '2016-06-23 03:46:21', NULL),
(61, 16, 1, 10, '2016-06-23 03:47:04', '2016-06-23 03:47:04', NULL),
(62, 16, 2, 9, '2016-06-23 03:47:06', '2016-06-23 03:47:06', NULL),
(63, 16, 3, 18, '2016-06-23 03:47:06', '2016-06-23 03:47:06', NULL),
(64, 17, 1, 10, '2016-06-23 03:48:36', '2016-06-23 03:48:36', NULL),
(65, 17, 2, 9, '2016-06-23 03:48:36', '2016-06-23 03:48:36', NULL),
(66, 17, 3, 18, '2016-06-23 03:48:36', '2016-06-23 03:48:36', NULL),
(67, 18, 1, 10, '2016-06-23 03:48:45', '2016-06-23 03:48:45', NULL),
(68, 18, 2, 9, '2016-06-23 03:48:45', '2016-06-23 03:48:45', NULL),
(69, 18, 3, 18, '2016-06-23 03:48:45', '2016-06-23 03:48:45', NULL),
(70, 19, 1, 10, '2016-06-23 03:50:13', '2016-06-23 03:50:13', NULL),
(71, 19, 2, 9, '2016-06-23 03:50:13', '2016-06-23 03:50:13', NULL),
(72, 19, 3, 18, '2016-06-23 03:50:13', '2016-06-23 03:50:13', NULL),
(73, 20, 1, 8, '2016-06-23 03:50:22', '2016-06-23 03:50:22', NULL),
(74, 20, 2, 7, '2016-06-23 03:50:23', '2016-06-23 03:50:23', NULL),
(75, 20, 3, 4, '2016-06-23 03:50:23', '2016-06-23 03:50:23', NULL),
(76, 21, 1, 8, '2016-06-23 03:50:25', '2016-06-23 03:50:25', NULL),
(77, 21, 2, 7, '2016-06-23 03:50:25', '2016-06-23 03:50:25', NULL),
(78, 21, 3, 4, '2016-06-23 03:50:26', '2016-06-23 03:50:26', NULL),
(79, 22, 1, 8, '2016-06-23 03:50:33', '2016-06-23 03:50:33', NULL),
(80, 22, 2, 7, '2016-06-23 03:50:33', '2016-06-23 03:50:33', NULL),
(81, 22, 3, 4, '2016-06-23 03:50:33', '2016-06-23 03:50:33', NULL),
(82, 23, 1, 18, '2016-06-23 03:50:45', '2016-06-23 03:50:45', NULL),
(83, 23, 2, 10, '2016-06-23 03:50:45', '2016-06-23 03:50:45', NULL),
(84, 23, 3, 1, '2016-06-23 03:50:45', '2016-06-23 03:50:45', NULL),
(85, 24, 1, 12, '2016-06-23 03:51:12', '2016-06-23 03:51:12', NULL),
(86, 24, 2, 19, '2016-06-23 03:51:12', '2016-06-23 03:51:12', NULL),
(87, 24, 3, 3, '2016-06-23 03:51:12', '2016-06-23 03:51:12', NULL),
(88, 25, 1, 12, '2016-06-23 03:51:16', '2016-06-23 03:51:16', NULL),
(89, 25, 2, 19, '2016-06-23 03:51:16', '2016-06-23 03:51:16', NULL),
(90, 25, 3, 3, '2016-06-23 03:51:16', '2016-06-23 03:51:16', NULL),
(91, 26, 1, 12, '2016-06-23 03:51:23', '2016-06-23 03:51:23', NULL),
(92, 26, 2, 19, '2016-06-23 03:51:23', '2016-06-23 03:51:23', NULL),
(93, 26, 3, 3, '2016-06-23 03:51:23', '2016-06-23 03:51:23', NULL),
(94, 27, 1, 12, '2016-06-23 03:51:48', '2016-06-23 03:51:48', NULL),
(95, 27, 2, 5, '2016-06-23 03:51:48', '2016-06-23 03:51:48', NULL),
(96, 27, 3, 20, '2016-06-23 03:51:48', '2016-06-23 03:51:48', NULL),
(97, 28, 1, 12, '2016-06-23 03:51:57', '2016-06-23 03:51:57', NULL),
(98, 28, 2, 5, '2016-06-23 03:51:57', '2016-06-23 03:51:57', NULL),
(99, 28, 3, 20, '2016-06-23 03:51:57', '2016-06-23 03:51:57', NULL),
(100, 29, 1, 14, '2016-06-23 03:52:18', '2016-06-23 03:52:18', NULL),
(101, 29, 2, 22, '2016-06-23 03:52:18', '2016-06-23 03:52:18', NULL),
(102, 29, 3, 2, '2016-06-23 03:52:18', '2016-06-23 03:52:18', NULL),
(103, 30, 1, 14, '2016-06-23 03:52:28', '2016-06-23 03:52:28', NULL),
(104, 30, 2, 22, '2016-06-23 03:52:28', '2016-06-23 03:52:28', NULL),
(105, 30, 3, 2, '2016-06-23 03:52:28', '2016-06-23 03:52:28', NULL),
(106, 31, 1, 8, '2016-06-23 03:52:38', '2016-06-23 03:52:38', NULL),
(107, 31, 2, 20, '2016-06-23 03:52:38', '2016-06-23 03:52:38', NULL),
(108, 31, 3, 13, '2016-06-23 03:52:38', '2016-06-23 03:52:38', NULL),
(109, 32, 1, 8, '2016-06-23 03:52:45', '2016-06-23 03:52:45', NULL),
(110, 32, 2, 20, '2016-06-23 03:52:45', '2016-06-23 03:52:45', NULL),
(111, 32, 3, 13, '2016-06-23 03:52:45', '2016-06-23 03:52:45', NULL),
(112, 33, 1, 11, '2016-06-23 03:52:55', '2016-06-23 03:52:55', NULL),
(113, 33, 2, 6, '2016-06-23 03:52:55', '2016-06-23 03:52:55', NULL),
(114, 33, 3, 18, '2016-06-23 03:52:55', '2016-06-23 03:52:55', NULL),
(115, 34, 1, 11, '2016-06-23 03:53:04', '2016-06-23 03:53:04', NULL),
(116, 34, 2, 6, '2016-06-23 03:53:04', '2016-06-23 03:53:04', NULL),
(117, 34, 3, 18, '2016-06-23 03:53:04', '2016-06-23 03:53:04', NULL),
(118, 35, 10, 19, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(119, 35, 11, 20, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(120, 35, 12, 15, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(121, 35, 13, 7, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(122, 35, 14, 5, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(123, 35, 15, 3, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(124, 35, 16, 17, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(125, 35, 17, 9, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(126, 35, 18, 11, '2016-06-23 03:53:25', '2016-06-23 03:53:25', NULL),
(127, 35, 19, 14, '2016-06-23 03:53:26', '2016-06-23 03:53:26', NULL),
(128, 36, 10, 19, '2016-06-23 03:53:33', '2016-06-23 03:53:33', NULL),
(129, 36, 11, 20, '2016-06-23 03:53:33', '2016-06-23 03:53:33', NULL),
(130, 36, 12, 15, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(131, 36, 13, 7, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(132, 36, 14, 5, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(133, 36, 15, 3, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(134, 36, 16, 17, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(135, 36, 17, 9, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(136, 36, 18, 11, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(137, 36, 19, 14, '2016-06-23 03:53:34', '2016-06-23 03:53:34', NULL),
(138, 37, 10, 9, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(139, 37, 11, 11, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(140, 37, 12, 19, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(141, 37, 13, 8, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(142, 37, 14, 2, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(143, 37, 15, 1, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(144, 37, 16, 17, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(145, 37, 17, 5, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(146, 37, 18, 20, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(147, 37, 19, 4, '2016-06-23 03:53:49', '2016-06-23 03:53:49', NULL),
(148, 38, 10, 13, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(149, 38, 11, 3, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(150, 38, 12, 7, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(151, 38, 13, 8, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(152, 38, 14, 16, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(153, 38, 15, 2, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(154, 38, 16, 20, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(155, 38, 17, 14, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(156, 38, 18, 11, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(157, 38, 19, 18, '2016-06-23 03:54:03', '2016-06-23 03:54:03', NULL),
(158, 39, 10, 1, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(159, 39, 11, 20, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(160, 39, 12, 18, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(161, 39, 13, 16, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(162, 39, 14, 15, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(163, 39, 15, 6, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(164, 39, 16, 19, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(165, 39, 17, 5, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(166, 39, 18, 7, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(167, 39, 19, 22, '2016-06-23 03:54:53', '2016-06-23 03:54:53', NULL),
(168, 40, 10, 1, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(169, 40, 11, 20, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(170, 40, 12, 18, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(171, 40, 13, 16, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(172, 40, 14, 15, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(173, 40, 15, 6, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(174, 40, 16, 19, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(175, 40, 17, 5, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(176, 40, 18, 7, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(177, 40, 19, 22, '2016-06-23 04:30:36', '2016-06-23 04:30:36', NULL),
(178, 41, 1, 14, '2016-06-23 07:19:18', '2016-06-23 07:19:18', NULL),
(179, 41, 2, 19, '2016-06-23 07:19:18', '2016-06-23 07:19:18', NULL),
(180, 41, 3, 22, '2016-06-23 07:19:18', '2016-06-23 07:19:18', NULL),
(181, 42, 10, 3, '2016-06-23 08:08:27', '2016-06-23 08:08:27', NULL),
(182, 42, 11, 17, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(183, 42, 12, 14, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(184, 42, 13, 10, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(185, 42, 14, 6, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(186, 42, 15, 21, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(187, 42, 16, 19, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(188, 42, 17, 15, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(189, 42, 18, 9, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(190, 42, 19, 22, '2016-06-23 08:08:28', '2016-06-23 08:08:28', NULL),
(191, 43, 4, 22, '2016-06-23 08:25:23', '2016-06-23 08:25:23', NULL),
(192, 43, 5, 5, '2016-06-23 08:25:23', '2016-06-23 08:25:23', NULL),
(193, 43, 6, 18, '2016-06-23 08:25:23', '2016-06-23 08:25:23', NULL),
(194, 43, 7, 17, '2016-06-23 08:25:24', '2016-06-23 08:25:24', NULL),
(195, 43, 8, 9, '2016-06-23 08:25:24', '2016-06-23 08:25:24', NULL),
(196, 43, 9, 12, '2016-06-23 08:25:24', '2016-06-23 08:25:24', NULL),
(197, 44, 4, 16, '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(198, 44, 5, 9, '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(199, 44, 6, 14, '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(200, 44, 7, 7, '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(201, 44, 8, 21, '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(202, 44, 9, 5, '2016-06-24 07:50:04', '2016-06-24 07:50:04', NULL),
(203, 45, 4, 16, '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(204, 45, 5, 9, '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(205, 45, 6, 14, '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(206, 45, 7, 7, '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(207, 45, 8, 21, '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(208, 45, 9, 5, '2016-06-24 07:50:52', '2016-06-24 07:50:52', NULL),
(209, 46, 1, 16, '2016-06-27 07:15:56', '2016-06-27 07:15:56', NULL),
(210, 46, 2, 9, '2016-06-27 07:15:56', '2016-06-27 07:15:56', NULL),
(211, 46, 3, 4, '2016-06-27 07:15:56', '2016-06-27 07:15:56', NULL),
(212, 47, 10, 17, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(213, 47, 11, 14, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(214, 47, 12, 1, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(215, 47, 13, 19, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(216, 47, 14, 6, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(217, 47, 15, 22, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(218, 47, 16, 4, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(219, 47, 17, 20, '2016-06-27 07:16:25', '2016-06-27 07:16:25', NULL),
(220, 47, 18, 18, '2016-06-27 07:16:26', '2016-06-27 07:16:26', NULL),
(221, 47, 19, 2, '2016-06-27 07:16:26', '2016-06-27 07:16:26', NULL),
(222, 48, 4, 8, '2016-06-27 07:18:40', '2016-06-27 07:18:40', NULL),
(223, 48, 5, 22, '2016-06-27 07:18:40', '2016-06-27 07:18:40', NULL),
(224, 48, 6, 15, '2016-06-27 07:18:40', '2016-06-27 07:18:40', NULL),
(225, 48, 7, 10, '2016-06-27 07:18:40', '2016-06-27 07:18:40', NULL),
(226, 48, 8, 13, '2016-06-27 07:18:40', '2016-06-27 07:18:40', NULL),
(227, 48, 9, 5, '2016-06-27 07:18:40', '2016-06-27 07:18:40', NULL),
(228, 49, 1, 3, '2016-06-28 03:10:03', '2016-06-28 03:10:03', NULL),
(229, 49, 2, 4, '2016-06-28 03:10:03', '2016-06-28 03:10:03', NULL),
(230, 49, 3, 9, '2016-06-28 03:10:03', '2016-06-28 03:10:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `last_login`, `last_ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(0, 'Guest', 'guest@mail.com', '$2y$10$KzFgAPmqVyE8.HE.ZUPyBemCwsYZmx/v7vnurEKbhvMHHzcrrNzFm', NULL, NULL, NULL, NULL, NULL, NULL),
(1, 'admin', 'admin@gmail.com', '$2y$10$Ulg8LX8z6gwfaE2Om4xRKe2glMcUIXETtbCt066P32Kk2ck4lX9/O', '3XKgSeEzFAnLwvlwYp3WC8pn30q3GvNQKFwHwjyqJ0uOvcJ7GdCfTAQ3emco', NULL, NULL, '2016-06-22 09:56:02', '2016-06-27 05:53:07', NULL),
(2, 'ainun', 'ainun@gmail.com', '$2y$10$KzFgAPmqVyE8.HE.ZUPyBemCwsYZmx/v7vnurEKbhvMHHzcrrNzFm', NULL, NULL, NULL, '2016-06-22 09:56:03', '2016-06-22 09:56:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `fullname`, `birthday`, `address`, `phone`, `gender`, `pic`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'administrator', '1992-01-26', 'jakarta pusat', '081448811190', 'laki-laki', 'ava-laki.png', '2016-06-22 09:56:03', '2016-06-22 09:56:03', NULL),
(2, 1, 'Siti Ainun', '1992-05-21', 'jakarta selatan', '08167990001', 'perempuan', 'ava-laki.png', '2016-06-22 09:56:03', '2016-06-22 09:56:03', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deck_card`
--
ALTER TABLE `deck_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `reading`
--
ALTER TABLE `reading`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reading_category_id_index` (`category_id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `result_user_id_index` (`user_id`);

--
-- Indexes for table `result_reading`
--
ALTER TABLE `result_reading`
  ADD PRIMARY KEY (`id`),
  ADD KEY `result_reading_result_id_index` (`result_id`),
  ADD KEY `result_reading_reading_id_index` (`reading_id`),
  ADD KEY `result_reading_deck_id_index` (`deck_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_profile_user_id_index` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `counter`
--
ALTER TABLE `counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deck_card`
--
ALTER TABLE `deck_card`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reading`
--
ALTER TABLE `reading`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `result_reading`
--
ALTER TABLE `result_reading`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reading`
--
ALTER TABLE `reading`
  ADD CONSTRAINT `reading_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `result_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `result_reading`
--
ALTER TABLE `result_reading`
  ADD CONSTRAINT `result_reading_deck_id_foreign` FOREIGN KEY (`deck_id`) REFERENCES `deck_card` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `result_reading_reading_id_foreign` FOREIGN KEY (`reading_id`) REFERENCES `reading` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `result_reading_result_id_foreign` FOREIGN KEY (`result_id`) REFERENCES `result` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `user_profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
