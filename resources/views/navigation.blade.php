<nav class="navbar" role="navigation">
    <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tarotku</a>
        </div>
        <div id="navbar-collapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}">Home</a></li>
                @if(!Auth::check())
                <li><a href="{{ url('auth/login') }}">Login</a></li>
                <li><a href="{{ url('auth/register') }}">Register</a></li>
                @else
                <li><a href="#">Profile</a></li>
                <li><a href="#">Histori Ramalan</a></li>
                <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                @endif
            </ul>
         </div><!--/.nav-collapse -->
    </div>
</nav>