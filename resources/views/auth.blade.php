<!DOCTYPE html>
<html lang="en">
<head>        
    <title>@yield('title', 'Default')</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    @yield('metadata')
    
    @yield('include')

</head>
<body> 
    
    <div id="whiteboard" class="container-fluid">

        @yield('header')

        @yield('navigation')

        @yield('content')

        <!-- @yield('footer')
 -->
    </div>

</body>
</html>