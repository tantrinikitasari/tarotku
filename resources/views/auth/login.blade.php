@extends('auth')

@section('title')Login @endsection

@section('include') 
    @include( 'Site::include' )
@endsection

@section('navigation') 
    @include( 'Site::navigation' )
@endsection

@section('content')
<div class="choose-outer">
    <div class="container">
        <div class="page-header">
            <form class="form-horizontal" method="POST" action="{{ url('auth/login') }}">
                {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="col-xs-6 col-md-4 col-md-offset-1 kanan">e-mail</label>
                            <div class="col-xs-6 col-md-4">
                                <input type="email" name="email" class="form-control input-sm" value="{{ old('email') }}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-6 col-md-4 col-md-offset-1 kanan">password</label>
                        <div class="col-xs-6 col-md-4">
                            <input type="password" name="password" class="form-control input-sm" id="password">
                        </div>
                    </div>                  
                    <div class="form-group">
                        <div class="col-xs-6 col-xs-offset-6 col-md-4 col-md-offset-5">
                            <input type="checkbox" name="remember"> Remember Me
                        </div>
                    </div>
                    <div class="form-group" >
                         <div class="col-xs-6 col-xs-offset-6 col-md-4 col-md-offset-5">
                            <button type="submit" class="btn btn-get-reading" >Login</button>
                        </div>
                    </div>
                    @if (count($errors))
                    <div class="form-group">
                        <div class="col-xs-6 col-xs-offset-6 col-md-4 col-md-offset-5">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </form>      
        </div>
    </div>
</div>
@endsection

@section('footer') 
    @include( 'Site::footer' )
@endsection