@extends('auth')

@section('title')Registration @endsection

@section('metadata')
    @include( 'Site::metadata' )
@endsection

@section('include') 
    @include( 'Site::include' )
@endsection

@section('navigation') 
    @include( 'Site::navigation' )
@endsection

@section('content')
<div class="choose-outer">
    <div class="container">
        <div class="choose-wrap">
            <form class="form-horizontal" method="POST" action="{{ url('auth/register') }}">
                {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Username</label>
                            <div class="col-md-4">
                                <input type="username"  class="form-control input-sm" name="username" value="{{ old('username') }}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">E-mail</label>
                            <div class="col-md-4">
                                <input type="email" name="email" class="form-control input-sm" value="{{ old('email') }}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Password</label>
                        <div class="col-md-4">
                            <input type="password" name="password" class="form-control input-sm" id="password">
                        </div>
                    </div>   
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Ulangi Password</label>
                        <div class="col-md-4">
                            <input type="password" name="password_confirmation" class="form-control input-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Nama Lengkap</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-sm" name="fullname">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Tanggal Lahir</label>
                            <div class="col-md-4" style="color:blueviolet;">
                                {!! Form::date('birthday', \Carbon\Carbon::now()) !!}
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Alamat</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-sm" name="address">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Jenis Kelamin</label>
                            <div class="col-md-4">
                                <input class="gender_button" type="radio" name="gender"
                                    <?php if (isset($gender) && $gender=="perempuan") echo "checked";?>
                                    value="perempuan"> Perempuan
                                    <input type="radio" name="gender"
                                    <?php if (isset($gender) && $gender=="laki-laki") echo "checked";?>
                                    value="laki-laki"> Laki-laki
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-1 kanan">Telepon</label>
                        <div class="col-md-4">
                            <input type="text" name="phone" class="form-control input-sm" >
                        </div>
                    </div>               
                    <div class="form-group" >
                         <div class="col-md-4 col-md-offset-5">
                            <button type="submit" class="btn btn-get-reading" >Daftar</button>
                        </div>
                    </div>
                    @if (count($errors))
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-5">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer') 
    @include( 'Site::footer' )
@endsection